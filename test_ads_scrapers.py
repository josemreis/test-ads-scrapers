import pathlib
import os
import json
from ads_scraper.utils import deploy_firefox
from ads_scraper.ads_scraper import AdsScraper
from ads_scraper.iframe_third_party_content_scraper import (
    IframeThirdPartyContentScraper,
)

# deploy the webdriver without opening the client
HEADLESS = True
# path to the test html file
TEST_FILE = "/".join(
    [
        "file://",
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

# TEST_FILE = "/".join(
#     [
#         "file://",
#         os.path.join(
#             pathlib.Path(__file__).parent.resolve(),
#             "resources/samples/AsahiShimbun/AsahiShimbun.html",
#         ),
#     ]
# )


if __name__ == "__main__":

    if not os.path.isdir("output"):
        os.mkdir("output")

    # create a webdriver
    driver = deploy_firefox(headless=HEADLESS)

    ## Run all
    ads_all = AdsScraper(
        url=TEST_FILE,
        driver=driver,
        use_gpt=True,
        use_filter_rule_scraper=True,
        use_iframe_third_party_content_scraper=True,
        debug=True,
    ).scrape_ads()

    with open("output/all.json", "w") as f:
        json.dump(ads_all, f, indent=4)

    ## Evaluate each method
    # create AdsScraper instance using gpt only
    ads_scraper_gpt = AdsScraper(
        url=TEST_FILE,
        driver=driver,
        use_gpt=True,
        use_filter_rule_scraper=False,
        use_iframe_third_party_content_scraper=False,
        debug=True,
    )
    # create AdsScraper instance using filter rules only
    ads_scraper_fr = AdsScraper(
        url=TEST_FILE,
        driver=driver,
        use_gpt=False,
        use_filter_rule_scraper=True,
        use_iframe_third_party_content_scraper=False,
        debug=True,
    )
    # create a iframe third-party content scraper instance
    ads_scraper_iframe = AdsScraper(
        url=TEST_FILE,
        driver=driver,
        use_gpt=False,
        use_filter_rule_scraper=False,
        use_iframe_third_party_content_scraper=True,
        debug=True,
    )

    # scrape ads using GPT
    _ads_gpt = ads_scraper_gpt.scrape_ads()
    # write
    with open("output/gpt_scraper_results.json", "w") as gpt_file:
        json.dump(_ads_gpt, gpt_file, indent=4)
    if _ads_gpt:
        ads_gpt = [_.get("url") for _ in _ads_gpt]
        pp_ads_gpt = "\n\t".join(ads_gpt)
    else:
        ads_gpt = []
        pp_ads_gpt = ""

    # scrape ads using ad blocker filter rules
    _ads_fr = ads_scraper_fr.scrape_ads()
    with open("output/filter_rules_scraper_results.json", "w") as fr_file:
        json.dump(_ads_fr, fr_file, indent=4)
    if _ads_fr:
        ads_fr = [_.get("url") for _ in _ads_fr]
        pp_ads_fr = "\n\t".join(ads_fr)
    else:
        ads_fr = []
        pp_ads_fr = ""

    # scrape ads using iframe third-party content scraper
    _ads_iframe = ads_scraper_iframe.scrape_ads()
    with open("output/iframe_scraper_results.json", "w") as iframe_file:
        json.dump(_ads_iframe, iframe_file, indent=4)
    if _ads_iframe:
        third_party_urls = [
            _.get("url") for _ in _ads_iframe if _.get("is_third_party")
        ]
        ads_iframe = [_.get("url") for _ in _ads_iframe if _.get("is_ad")]
        pp_ads_iframe = "\n\t".join(ads_iframe)
        pp_third_party_urls = "\n\t".join(third_party_urls)
    else:
        third_party_urls = [] 
        ads_iframe = []
        pp_ads_iframe = pp_third_party_urls = ""

    # kill the webdriver
    driver.close()
    driver.quit()

    # print the results
    print("\n\n> Results:\n")
    print(
        "> Using Google Publisher Tag Manager, found {} out of the expected 5 ads{}".format(
            len(ads_gpt), ":\n\t" + pp_ads_gpt if len(ads_gpt) else "."
        )
    )
    print(
        "> Using Filter Rules xpath matching, found {} out of the expected 5 ads{}".format(
            len(ads_fr), ":\n\t" + pp_ads_fr if len(ads_fr) else "."
        )
    )
    print(
        "> Using the iframes third party content scraper, found {} ads out of the expected 5 ads{}".format(
            len(ads_iframe), ":\n\t" + pp_ads_iframe if len(ads_iframe) else "."
        )
    )
    print(
        "> Using the iframes third party content scraper, found {} third party urls{}".format(
            len(third_party_urls),
            ":\n\t" + pp_third_party_urls if len(third_party_urls) else ".",
        )
    )
