# A test repo for a minimal version of the ads-scraper

## Instalation and usage

1. Install the required dependencies

```python
pip3 install -r requirements.txt
```

2. If missing, download the latest geckodriver to the `resources` dir. If you want to use your own instance, replace the constante `PATH_TO_GECKODRIVER` in `ads_scraper/constants.py`.

```python
python3 scripts/geckodriver.py
```

3. Run the test script. This script will attempt to collect ads from a website stored in `resources/samples/compressortech2`. This website has 5 ads and uses the [Google Publisher Tag Manager API (GPT)](https://developers.google.com/publisher-tag).

```python
python3 test_ads_scrapers.py
```
4. The script will print out the relevant results as well as store the ads data identified in the `output` dir. It will save one json file per retrieval method used. Below an example of the output generated from collecting the ads using the `AdsScraper`:

```bash
[
    {
        "url": "https://vistra.hoerbiger.com/?mtm_campaign=vistra2022&mtm_kwd=mpu1&mtm_source=ct2magazine&mtm_medium=paidad&mtm_content=desktop",
        "is_ad": true,
        "is_third_party": true,
        "website_visited": "file:////home/jr/Dropbox/python_default/test_ads_scraper/resources/samples/compressortech2/compressortech2.html",
        "visited_at": "2022-06-28T07:44:31.754634",
        "crawler_id": "crawler_28_40_2022_07_40_38",
        "methods_used_for_ad_identification": {
            "gpt": true,
            "filter_rules_scraper": true,
            "iframe_scraper": true
        }
    },
    {
        "url": "https://www.cpicompression.com/",
        "is_ad": true,
        "is_third_party": true,
        "website_visited": "file:////home/jr/Dropbox/python_default/test_ads_scraper/resources/samples/compressortech2/compressortech2.html",
        "visited_at": "2022-06-28T07:44:31.754790",
        "crawler_id": "crawler_28_40_2022_07_40_38",
        "methods_used_for_ad_identification": {
            "gpt": true,
            "filter_rules_scraper": true,
            "iframe_scraper": true
        }
    },
    {
        "url": "https://khldatacloud.com/registration?sourcecode=CT2Onlinereg",
        "is_ad": true,
        "is_third_party": true,
        "website_visited": "file:////home/jr/Dropbox/python_default/test_ads_scraper/resources/samples/compressortech2/compressortech2.html",
        "visited_at": "2022-06-28T07:44:31.754800",
        "crawler_id": "crawler_28_40_2022_07_40_38",
        "methods_used_for_ad_identification": {
            "gpt": true,
            "filter_rules_scraper": true,
            "iframe_scraper": true
        }
    },
    {
        "url": "https://www.gastechevent.com/conferences/strategic-conference/?utm_source=COMPRESSORtech2&utm_medium=web+banner&utm_campaign=Gastech+2022&utm_id=delprom&utm_term=May&utm_content=Early+bird",
        "is_ad": true,
        "is_third_party": true,
        "website_visited": "file:////home/jr/Dropbox/python_default/test_ads_scraper/resources/samples/compressortech2/compressortech2.html",
        "visited_at": "2022-06-28T07:44:31.754810",
        "crawler_id": "crawler_28_40_2022_07_40_38",
        "methods_used_for_ad_identification": {
            "gpt": true,
            "filter_rules_scraper": true,
            "iframe_scraper": true
        }
    },
    {
        "url": "https://www.arielcorp.com/Campaigns/pledge.html?utm_source=CT2&utm_medium=Banner-Ad&utm_campaign=pledge",
        "is_ad": true,
        "is_third_party": true,
        "website_visited": "file:////home/jr/Dropbox/python_default/test_ads_scraper/resources/samples/compressortech2/compressortech2.html",
        "visited_at": "2022-06-28T07:44:31.754818",
        "crawler_id": "crawler_28_40_2022_07_40_38",
        "methods_used_for_ad_identification": {
            "gpt": true,
            "filter_rules_scraper": true,
            "iframe_scraper": true
        }
    }
]
```

## `AdsScraper`

The ads scraper methodology relies on a main class: `AdsScraper`.

The class `AdsScraper` uses selenium and several different methods to identify online ads and extract their urls as well as, when possible. As of now, the module makes use of three methods for identifying online ads:

1. Using the [Google Publisher Tag Manager API (GPT)](https://developers.google.com/publisher-tag) to identify the xpaths of ad containers
2. Matching web-element xpaths against xpaths from [ad blocker filter rules](ads_scraper/constants.py)
3. Using href and src attributes in elements within elements inside `<iframes>`

### Usage

Using the three methods.

```python
from ads_scraper.utils import deploy_firefox
from ads_scraper.ads_scraper import AdsScraper

# deploy the webdriver without opening the client
HEADLESS = True
# target url
TARGET_URL = "https://www.asahi.com/ajw/"

# create a webdriver
driver = deploy_firefox(headless=HEADLESS)

## Run all
ads_all = AdsScraper(
    url=TARGET_URL,
    driver=driver,
    use_gpt=True,
    use_filter_rule_scraper=True,
    use_iframe_third_party_content_scraper=True,
    debug=True,
).scrape_ads()

print(ads_all)

# kill the webdriver
driver.close()
driver.quit()
```
### Ad identification methods in more detail

#### GPT scraper

This is a method for identifying xpaths of web-elements containing ads.This method leverages [Google Publisher Tag's API](https://developers.google.com/publisher-tag/reference#googletag.Slot_getTargeting) for retrieving the ids of the web-elements with the ad containers. Return their xpaths.

1.  Visit a page and check if the google publisher tag manager api is loaded and ready to be called. If not skip and assign None;
2.  If yes, fetch all the id attributes of ad slots managed by google publisher tag manager, using their api we could get the slot id for the first ad slot by running this js code. `var ad_id = googletag.pubads().getSlots(0)[0].getSlotElementId();`
3.  Generate an xpath using the element IDs
4.  Return so it can be used by `AdsScraper`

##### Example usage

```python
import time
import os
import pathlib
from ads_scraper.utils import deploy_firefox
from ads_scraper.gpt_scraper import GooglePubTagScraper

TEST_FILE = "/".join(
    [
        "file://",
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

driver = deploy_firefox(headless=False)

driver.get(TEST_FILE)

time.sleep(10)
gpt_scraper = GooglePubTagScraper(driver = driver)

xpaths = gpt_scraper.get_ad_container_xpaths()
print(xpaths)

driver.close()
driver.quit()
```

##### Overall performance

When websites use GPT, it performs very well. In the given test case (`/resources/samples/compressortech2/compressortech2.html`), it successfully identifies all 5 ads.

##### Known Issues

1. Not all websites use this service. You can easily check if they use it by calling the following method `GooglePubTagScraper.is_console_active()`.
2. Whether or not a website uses this service does not seem to be random. Might bias the analysis.

#### Filter rule scraper

This is a method for identifying xpaths of web-elements containing ads using filter rules (see `ads_scraper/contants.py` and `resources/filter_rules`).

1. Generate a regular expression from filter rules from sources such as EasyList or ad guard converted to xpaths.
2.  Fetch web-elements matching that patter;
3.  For each match, store the web-element and fetch the parent node and the direct children;
4.  Fetch the xpath of the above mentioned elements, and return.

##### Usage

```python
import codecs
import os
import pathlib
from ads_scraper.filter_rule_scraper import FilterRulesAdsScraper
from ads_scraper.constants import (
    FILTER_RULES_URLS_ALL,
)

TEST_FILE = "/".join(
    [
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

with codecs.open(TEST_FILE, "r") as f:
    html_code = f.read()

ads_parser = FilterRulesAdsScraper(
    html_source=html_code,
    filter_rules_url=FILTER_RULES_URLS_ALL,
    filter_rules_filename="filter_rules_all",
)

xpaths = ads_parser.find_ads_xpath()
print(xpaths)
```

##### Overall performance

1. Bad. It does not identify any of the 5 ads present in the current test case (`/resources/samples/compressortech2/compressortech2.html`).
2. slower

##### Known issues

1. It does not work and it is unclear why.

#### Iframe scraper

This method identifies third party urls within iframes, passess them by ad blocker filter rules so as to code whether or not they are ads, and clicks on them to fetch the landing page. 

1.  Scroll the complete page and identify all iframe elements in a page;
2.  For each, extract all the src or href attributes;
3.  Check if the PS+1 identified in URLs of the src/href attributes is different from that of the page we are visiting, mark as third-party if not identical;
4.  Pass all the urls by  filter rules so as to detect whether ad blockers would classify it as an ad using `adblockparser.AdblockRules`. Keep the results.
5.  For each third-party url, regardless of whether it was classified as an ad or not, click on it, extract the url and page source of the landing page (ideally we archive it for size constraints…) and store it. Return a list of dicts with these features and the result of whether it was classified as an ad.

##### Usage

```python
import time
import os
import pathlib
from ads_scraper.utils import deploy_firefox
from ads_scraper.iframe_third_party_content_scraper import (
    IframeThirdPartyContentScraper,
)

TEST_FILE = "/".join(
    [
        "file://",
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

driver = deploy_firefox(headless=False)

driver.get(TEST_FILE)

time.sleep(10)
iframe_scraper = IframeThirdPartyContentScraper(
    driver=driver, url=TEST_FILE, repeat_n_times=3
)

ads_and_third_party_content = iframe_scraper.scrape_third_party_content()

print(ads_and_third_party_content)

driver.close()
driver.quit()
```

##### Overall performance

* Very good at identifying third-party content in the test case (`/resources/samples/compressortech2/compressortech2.html`). It identifies 5 different third-party urls which match the 5 ads present in the page. 
* The ad classifier miss-classifies third-party content as not being ads (got 1 out of 5).

##### Known Issues

1. Filter-rules seem not to be enough for classiying whether the third-party url is an ad, as they miss many.

