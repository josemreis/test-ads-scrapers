﻿var articleswipers = new Array();
$(function () {
    $('.swiper-container').each(function () {
        var idselector = '#' + $(this).attr('id');
        var slidespergroup = Math.floor(($(idselector).width() - 60) / 230);
        if (slidespergroup == 0) {
            slidespergroup = 1;
        }
        var swiperitem = new Object();
        swiperitem.id = idselector;
        swiperitem.swiper = new Swiper(idselector, {
            slidesPerView: 'auto',
            spaceBetween: 10,
            slidesPerGroup: slidespergroup,
            freeMode: true,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 70,
            navigation: {
                nextEl: '.custswiper-button-next',
                prevEl: '.custswiper-button-prev'
            }
        });
        articleswipers.push(swiperitem);
    });
});

$(window).resize(function () {
    $.each(articleswipers, function (index, item) {
        var slidespergroup = Math.floor(($(item.id).width() - 60) / 230);
        if (slidespergroup == 0) {
            slidespergroup = 1;
        }
        item.swiper.params.slidesPerGroup = slidespergroup,
            item.swiper.init();
    });
});