// ブラウザがieかどうか判定（EBDA除外用）
var asaAdUaLC = window.navigator.userAgent.toLowerCase();
if (asaAdUaLC.indexOf('msie') != -1 || asaAdUaLC.indexOf('trident') != -1) {
  var asaAdBrowserIE = "1";
} else {
  var asaAdBrowserIE = "";
}

googletag.cmd.push(function() {
  googletag.defineSlot('/57465213/www.asahi.com/english/pc/1st_rect', [[300, 600], [300, 250]], 'div-gpt-ad-1518226518415-0').addService(googletag.pubads());
  googletag.defineSlot('/57465213/www.asahi.com/english/pc/2nd_rect', [300, 250], 'div-gpt-ad-1518226547288-0').addService(googletag.pubads());
  googletag.defineSlot('/57465213/www.asahi.com/english/pc/footer_rect_l', [300, 250], 'div-gpt-ad-1518226592045-0').addService(googletag.pubads());
  googletag.defineSlot('/57465213/www.asahi.com/english/pc/footer_rect_r', [300, 250], 'div-gpt-ad-1518226653270-0').addService(googletag.pubads());
  (asaAdBrowserIE) ? googletag.pubads().setTargeting("Browser_IE", asaAdBrowserIE) : null;
  googletag.pubads().enableSingleRequest();
  googletag.pubads().collapseEmptyDivs();
  googletag.enableServices();
});