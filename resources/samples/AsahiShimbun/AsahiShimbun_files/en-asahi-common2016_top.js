/*-----------------------------------
The Asahi Shimbun(En)
　--　Common JS　--

last modified --2016-02-02 (new)
-----------------------------------*/

// パス設定の問題で、呼び元のテンプレートで実行
// var d = document;
// d.write('<script type="text/javascript" src="/ajw/js/jquery-1.8.3.min.utf8.js"></script>');
// d.write('<script type="text/javascript" src="/ajw/js/en-asahi-site2016.js"></script>');
// d.write('<script type="text/javascript" src="/ajw/js/en-asahi-header2016.js"></script>');
//
// var topDeviceChk_ua = navigator.userAgent;
// if (topDeviceChk_ua.indexOf('Linux; U; Android ') != -1 || topDeviceChk_ua.indexOf('Linux; Android ') != -1 || topDeviceChk_ua.indexOf('Android; Mobile; ') != -1 || topDeviceChk_ua.indexOf('Android; Tablet; ') != -1 || topDeviceChk_ua.indexOf('iPhone') != -1 ) {
// 	alert('ok?');
// 	d.write('<script type="text/javascript" src="/ajw/js/en-asahi-smartlink2016_top.js"></script>');
// }