/*-----------------------------------
The Asahi Shimbun(En)
　--　ヘッダー JS　--

last modified --2016-02-02-(new)
-----------------------------------*/

/* グローバルナビ Dropdown
==================== */
/*$(function(){*/
/*	var GnaviLinkArray = {
		'Topnews':'http://www.asahi.com/?iref=com_gnavi',
		'Sports':'http://www.asahi.com/sports/?iref=com_gnavi',
		'Culture':'http://www.asahi.com/culture/?iref=com_gnavi',
		'Special':'http://www.asahi.com/special/?iref=com_gnavi',
		'Opinion':'http://www.asahi.com/opinion/?iref=com_gnavi',
		'Multimedia':'http://www.asahi.com/multimedia/?iref=com_gnavi',
		'Shimen':'http://www.asahi.com/shimen/?iref=com_gnavi',
		'Customize':'http://digital.asahi.com/customize/?iref=com_gnavi'
	};*/

	//ビルボード広告用
/*	var _ad_BILLBOARD_MIN_HEIGHT = 90;
	var _ad_BILLBOARD_ANIMATE_WAITE = 0;
	var _ad_BILLBOARD_FORCE_STOP = false;
	var _ad_BILLBOARD_FLG = false;
	var FavoritesBlockTimer;*/

/*	$("#GlobalNav #GlobalNavInner .MainNav .Nav li a").attr('href','javascript:void(0);');
	$("#GlobalNav #GlobalNavInner .MainNav .Nav li a").click(function(){
		var select_menu = $(this).attr('id').replace('Nav','');
		var menu_class  = ($(this).attr('class') !=undefined) ? $(this).attr('class') : '';
		if(menu_class.indexOf('Selected',0) !=-1 ){
			GnaviLink.cookieSet($(this).attr('id'));
			SubNaviLink.cookieClear();
			$(this).attr('href',GnaviLinkArray[select_menu]);
		} else {
			$(this).attr('href','javascript:void(0);');
			var menu_id     = '#GlobalNav #GlobalNavInner .MainNav';
			var menu_status = $('#GlobalNav #GlobalNavInner .SubNav:visible').html();
			var prev_menu   = (menu_status) ? $(menu_id + ' a.Selected').attr('id').replace('Nav','') : "";
			$('#GlobalNav #GlobalNavInner .MainNav .Nav li a.Selected').removeClass('Selected');
			//GnaviLink.cookieSet($(this).attr('id'));
			//SubNaviLink.cookieClear();
			if((select_menu == prev_menu) && menu_status){
				$('#GlobalNav #GlobalNavInner .SubNav:visible').slideUp(0);
				$('#GlobalNav #GlobalNavInner .SubNav .SubDropdown .Nav li').animate({opacity:'hide'},{duration:800},{easing:'swing'});
			} else {
				$('#' + GnaviLinkName + 'DD').slideUp(0,function(){$("style#ActiveControl").remove();});
				if($.fn.jquery==='1.4.4'){

					(menu_status) ? $('#GlobalNav #GlobalNavInner .SubNav:visible').slideUp(0,function(){
						$('#' + select_menu + 'DD').slideDown(0);
					}) : $('#' + select_menu + 'DD').slideDown(0);

					$('#' + select_menu + 'DD').css("display","block");

					switch(select_menu){
						case 'Customize':
							//Customize_event('hidden');
							//viewSelectedHtml();
							break;
					};

				}else if($.fn.jquery==='1.8.3'){
					var targetEle = this;
					var targetAnimate = 0;
					var targetOuterHeight = 0;*/
/*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+
ビルボード用制御
*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*/
/*					if( $("#ad_BILLBOARD *").size() > 0){
						$(".MainNav *").css("cursor","pointer");//二重押し対策
						clearInterval(FavoritesBlockTimer);//二重押し対策
						if( Number($("#HometopAd").outerHeight(true)) > _ad_BILLBOARD_MIN_HEIGHT){
							if( (!_ad_BILLBOARD_FLG && select_menu != "Customize" || _ad_BILLBOARD_FORCE_STOP )){//初回or砂時計解除後
								SubNaviSlideUp(menu_status,select_menu);
							}else{
								$(targetEle).css("cursor","progress");
								FavoritesBlockTimer=setInterval(function(){
									var now = Number($("#HometopAd").outerHeight(true));
									if(now <= _ad_BILLBOARD_MIN_HEIGHT || _ad_BILLBOARD_FORCE_STOP ){
										SubNaviSlideUp(menu_status,select_menu);
										$(targetEle).css("cursor","pointer");
										_ad_BILLBOARD_FLG = true;
										clearInterval(FavoritesBlockTimer);
									}else{
										if($("#ad_BILLBOARD embed").size() == 0){
											//flash 同じ高さを2秒保持したら1秒後に砂時計解除
											if( targetOuterHeight === now ){
												if( _ad_BILLBOARD_ANIMATE_WAITE <= targetAnimate ){
													_ad_BILLBOARD_FORCE_STOP = true;
												}
												targetAnimate++;
											}else{
												targetAnimate = 0;
											}
											targetOuterHeight = now;
										}
									}
								},1000);
							}
						}else{
							SubNaviSlideUp(menu_status,select_menu);
						}*/
/*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+
ゲート広告用制御
*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*-+*/
/*					}else if( $("#ad_GATE *").size() > 0){
						//Lite or Guest
						if(asa12_mode <= 1){
							(menu_status) ? $('#GlobalNav #GlobalNavInner .SubNav:visible').slideUp(0,function(){
								$('#' + select_menu + 'DD').slideDown(0);
							}) : $('#' + select_menu + 'DD').slideDown(0);

							$('#' + select_menu + 'DD').css("display","block");
							switch(select_menu){
								case 'Customize':
									//Customize_event('hidden');
									//viewSelectedHtml();
									break;
								default:
									//Customize_event();
									//viewSelectedHtml();
								break;
							};
						}else{
							SubNaviSlideUp(menu_status,select_menu);
						}
					}else{
						SubNaviSlideUp(menu_status,select_menu);
					}
				}else{
					(menu_status) ? $('#GlobalNav #GlobalNavInner .SubNav:visible').slideUp(0,function(){$('#' + select_menu + 'DD').slideDown(0);}) : $('#' + select_menu + 'DD').slideDown(0);
				}
				$('#GlobalNav #GlobalNavInner .SubNav .SubDropdown .Nav li').animate({opacity:'show'},{duration:0},{easing:'swing'});
				$(this).addClass("Selected");
			}
		}
	});*/




/*	function SubNaviSlideUp(menu_status,select_menu){
		var eleDD = $('#' + select_menu + 'DD');

		(menu_status) ? $('#GlobalNav #GlobalNavInner .SubNav:visible').slideUp(0,function(){
			eleDD.slideDown(0);
			eleDD.html(eleDD.html());
		}) : eleDD.slideDown(0);eleDD.html(eleDD.html());

		$('#' + select_menu + 'DD').css("display","block");
		switch(select_menu){
			case 'Customize':
				//Customize_event();
				//viewSelectedHtml();
			break;
		}
		return true;
	}*/

	//カスタマイズ－お気に入りボタン横のボタンの動作
	//利用停止 2015.05.18
/*	function Customize_event(flg){
		if(flg == 'hidden'){
			$("li.ToFavorites").addClass("NG144");
			$("span.SlideBtn").hide();
			$("#SelectDD").hide();
			if($("#CustomizeDD .SubDropdown ul.Nav").hasClass("NavOpen")){
				$("#CustomizeDD .SubDropdown ul.Nav").removeClass("NavOpen");
			}
		}else{
			$("#SelectDD").show();
			if( $("#CustomizeDD .SubDropdown ul.Nav").hasClass("NavOpen")){
				$("#CustomizeDD .SubDropdown ul.Nav").removeClass("NavOpen");
			}else{
				$("#CustomizeDD .SubDropdown ul.Nav").addClass("NavOpen");
			}
			$("span.SlideBtn").click(function(){
				$("#SelectDD").toggle(0,function(){
					if($("#CustomizeDD .SubDropdown ul.Nav").hasClass("NavOpen")){
						$("#CustomizeDD .SubDropdown ul.Nav").removeClass("NavOpen");
					}else{
						$("#CustomizeDD .SubDropdown ul.Nav").addClass("NavOpen");
					}
				});
			});
		}
	}*/

	/*お気に入り連載ヘッダー表示*/
	//利用停止 2015.05.18
/*	function viewSelectedHtml() {
		var digitalDomain = 'digital.asahi.com';
		var targetObject = $("#SelectDD ul.FavoritesBlock");
		var html  = '<li class="NoSelect">よく読む連載を登録すると、ページの上部に表示し、連載が更新されるとメールでお知らせする機能です。</li>';
		if ($().jquery === '1.8.3' && $.cookie("WWW_SESS") && targetObject.size() > 0) {
			reg = new RegExp(digitalDomain + "$");
			if (location.hostname.search(reg) !== -1) {//digital領域からのアクセス
				var settingJson = {
					url:'/customize/header/selected_favorite_json.php',
					//type: 'post',
					//dataType: 'json',
					cache: false,
					success: function(result, dataType) {
						result = eval( '(' + result + ')' );
						if (result.stat === 'ok') {
							html = result.html;
						}
					},
					error: function(request, status, error) {
						//console.log(XMLHttpRequest, textStatus, errorThrown);
					},
					complete: function() {
						targetObject.html(html);//HTMLを表示
					}
				};
				$.ajax(settingJson);

			} else if (location.hostname.search(/\.asahi\.com$/) !== -1) {//digital以外のasahi.com領域からのアクセス
				var settingJsonp = {
					url:'http://' + digitalDomain + '/customize/header/selected_favorite_jsonp.php',
					//type: 'post',
					dataType: 'jsonp',
					jsonpCallback: 'getSelectedHtml',
					success: function(result, dataType) {
						html = result;
					},
					error: function(request, status, error) {
						//console.log(XMLHttpRequest, textStatus, errorThrown);
					},
					complete:function() {
						targetObject.html(html);//HTMLを表示
					}
				};
				$.ajax(settingJsonp);
			} else {
				targetObject.html(html);//HTMLを表示
			}
		} else {
			targetObject.html(html);//HTMLを表示
		}
	};

	$("div#CustomizeDD").ready(function(){
		 if($("div#CustomizeDD").css("display") == 'block'){
				$("span.SlideBtn").click(function(){
					if($("ul.FavoritesBlock li.loading").size() === 1 ){
						$("#CustomizeDD .SubDropdown ul.Nav").addClass("NavOpen");
						$("#SelectDD").show();
						//viewSelectedHtml();
					}else{
						$("#SelectDD").toggle(0,function(){
							if($("#CustomizeDD .SubDropdown ul.Nav").hasClass("NavOpen")){
								$("#CustomizeDD .SubDropdown ul.Nav").removeClass("NavOpen");
							}else{
								$("#CustomizeDD .SubDropdown ul.Nav").addClass("NavOpen");
							}
						});
					}
				});

			}
		});
	});*/




/* 言語切り替え系 Click
==================== */
$(function(){
  //ON、OFFアクション定義
  $("div#LangNav").click(function(){
    if($("div#LangDD").css("display") =='none'){
      $("div#LangDD").show();
    }else{
      $("div#LangDD").hide();
    }
  });
  //範囲外クリックアクション定義
  $(document).click(function(e){
    if($("div#LangDD").size() > 0){
	    if(!$.contains($("div#LangNav")[0], e.target)){
	      $("div#LangDD").hide();
	    }
		}
  });
});

/* GnaviLink 制御
==================== */
/*var GnaviLink ={
  // 現在クッキー確認関数
  nowCookie:function(){
    return $.cookie("GnaviLink");
  },
  // クッキー設定関数
  cookieSet:function( Genre ){
    var cookieExpires = 7;
    var cookieDomain = "asahi.com";
    $.cookie("GnaviLink", Genre, {path:'/',expires:cookieExpires,domain:cookieDomain});
  },
  cookieClear:function(){
    var date = new Date();
    var cookieExpires = 0;
    var cookieDomain = "asahi.com";
    $.cookie("GnaviLink","",{path:'/',expires:cookieExpires,domain:cookieDomain});
  }
}*/

/* SubNaviLink 制御
==================== */
/*var SubNaviLink ={
  // 現在クッキー確認関数
  nowCookie:function(){
    return $.cookie("SubNaviLink");
  },
  // クッキー設定関数
  cookieSet:function( Genre ){
    var cookieExpires = 7;
    var	cookieDomain = "asahi.com";
    $.cookie("SubNaviLink", Genre, {path:'/',expires:cookieExpires,domain:cookieDomain});
  },
  cookieClear:function(){
    var date = new Date();
    var cookieExpires = 0;
    var cookieDomain = "asahi.com";
    $.cookie("SubNaviLink","",{path:'/',expires:cookieExpires,domain:cookieDomain});
  }
}*/

/* CSS 出し分け
==================== */
/*var GnaviLinkID = GnaviLink.nowCookie();
var SubNaviLinkID = SubNaviLink.nowCookie();
if(GnaviLinkID != null ){
	GnaviLinkName = GnaviLinkID.replace('Nav','');
	document.write('<style>#TopnewsDD{display:none;}</style>');
	document.write('<style id="ActiveControl">#' + GnaviLinkName + 'DD{display:block!important;}</style>');
}*/
$(function() {
	var genre_name = $("#Main .BreadCrumb .Genre").text();
	$("#GlobalNavInner .MainNav ul.Nav>li").each(function(index, element) {
		if (($(element).children("a").text() === genre_name) && genre_name != "") {
			$(element).addClass("Active");
			return false;
		}
	});
});