/*-----------------------------------
The Asahi Shimbun(En)
--　サイト・コンテンツ JS　--
en-asahi-site2016.js

last modified --2015-12-16-12:00--(写真台座ページをen化)

-----------------------------------*/

/* jQuery Cookie
 * https://github.com/carhartl/jquery-cookie
==================== */
(function($) {
    $.cookie = function(key, value, options) {
        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);
            if (value === null || value === undefined) {
                options.expires = - 1;
            }
            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }
            value = String(value);
            return (document.cookie = [
            encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
            // use expires attribute, max-age is not supported by IE
            options.expires ? '; expires=' + options.expires.toUTCString() : '',
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
            ].join(''));
        }
        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) {
            return s;
        }
        : decodeURIComponent;
        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
            if (decode(pair[0]) === key)
                return decode(pair[1] || '');
        }
        return null;
    };
})(jQuery);

/* 非会員/会員CSS切り替え(2014-10-01切り替え)
==================== */
var asa12_sessck = asa12_getCookie('WWW_SESS');
var asa12_modemei = '';
var asa12_mode = 0;
var asa14_mode = 0;
var asa14_coupon = 0;
if (asa12_sessck) {
    asa12_modemei = asa12_sessck.substring(0, 6);
    switch (asa12_modemei) {
    case 'OTHECA':
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-member2014.utf8.css" media="screen,print" />');
        asa12_mode = 2;
        asa14_mode = 2;
        asa14_coupon = 0;
        break;
    case 'OTHECT':
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-coupon2014.utf8.css" media="screen,print" />');
        asa12_mode = 2;
        asa14_mode = 2;
        asa14_coupon = 1;
        break;
    case 'LITECA':
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-lite2014.utf8.css" media="screen,print" />');
        asa12_mode = 1;
        asa14_mode = 1;
        asa14_coupon = 0;
        break;
    case 'OOUTCA':
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-oout2014.utf8.css" media="screen,print" />');
        asa12_mode = 0;
        asa14_mode = - 2;
        asa14_coupon = 0;
        break;
    case 'LOUTCA':
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-lout2014.utf8.css" media="screen,print" />');
        asa12_mode = 0;
        asa14_mode = - 1;
        asa14_coupon = 0;
        break;
    default:
        document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-nonregi2014.utf8.css" media="screen,print" />');
        asa12_mode = 0;
        asa14_mode = 0;
        asa14_coupon = 0;
        break;
    }
} else {
    document.write('<link rel="stylesheet" type="text/css" href="/ajw/css/asahi-nonregi2014.utf8.css" media="screen,print" />');
    asa12_mode = 0;
    asa14_mode = 0;
    asa14_coupon = 0;
}
function asa12_getCookie(theName) {
    theName += "="; // = を追加
    var theCookie = document.cookie + ";"; // 検索時最終項目で-1になるのを防ぐ
    var start = theCookie.indexOf(theName); // 指定されたセクション名を検索する
    if (start != - 1) {
        var end = theCookie.indexOf(";", start); // データを抜きだす
        return unescape(theCookie.substring(start + theName.length, end));
    }
    return false;
}


/* 非会員/会員スイッチ切り替え（class="SW"）
==================== */
function GetBrowser()
{
    if (navigator.userAgent.indexOf('Chrome') != - 1)
    {
        return 'Chrome';
    }
    if (navigator.userAgent.indexOf('Safari') != - 1)
    {
        return 'Safari';
    }
    if (navigator.userAgent.indexOf('Opera') != - 1)
    {
        return 'Opera';
    }
    if (navigator.userAgent.indexOf('MSIE') != - 1)
    {
        return 'IE';
    }
    if (navigator.userAgent.indexOf('Firefox') != - 1)
    {
        return 'FireFox';
    }
    return 'Unknown';
}
function clickAction(event, ele)
{
    var switchName = '';
    if (getClass = ele.className.split(" ")) {
        for ( i = 0 ; i < getClass.length; i ++) {
            if (getClass[i] == 'SW') {
                switchName = 'SW';
                break;
            }
        }
    }
    if (ele.href) {
        if (asa12_mode === 2 && switchName == 'SW')
        {
            //▼イベント無効化▼
            if (GetBrowser() == 'IE')
            {
                window.event.returnValue = false;
            } else
            {
                if (GetBrowser() == 'FireFox')
                {
                    event.preventDefault();
                } else {
                    window.event.preventDefault();
                }
            }
            //▲イベント無効化▲
            url = ele.href.match("[^/]*$");
            location.href = "http://digital.asahi.com/articles/" + url;
        }
    }
}
$(function() {
    if (GetBrowser() != 'IE') {
        if (asa12_mode === 2) {
            $("a.SW").each(function(i) {
                url = $(this).attr("href").match("[^/]*$");
                $(this).attr("href", "http://digital.asahi.com/articles/" + url);
            });
        }
    } else {
        $("a.SW").each(function(i) {
            $(this).attr("onclick", "");
        });
    }
});

/* フォントサイズの変更
 * http://web-mugen.com/javascript/jquerynochangecssfontsizechange/
==================== */
(function($) {
    $(function() {
        fontsizeChange();
    });
    function fontsizeChange() {
        var changeArea = $("#MainInner,#SubInner,#MainWrapInner"); // フォントサイズ変更エリア
        var btnArea = $("#FontSizeChange"); // フォントサイズ変更ボタンエリア
        var changeBtn = btnArea.find(".ChangeBtn"); // フォントサイズ変更ボタン
        var fontSize = [110, 100, 90]; // フォントサイズ（HTMLと同じ並び順、幾つでもOK、単位は％）
        var ovStr = "_ov"; // ロールオーバー画像ファイル末尾追加文字列（ロールオーバー画像を使用しない場合は値を空にする）
        var activeClass = "ChangeBtnActive"; // フォントサイズ変更ボタンのアクティブ時のクラス名
        var defaultSize = 1; // 初期フォントサイズ設定（HTMLと同じ並び順で0から数値を設定）
        var cookieExpires = 7; // クッキー保存期間
        var cookieDomain = "asahi.com"; // クッキードメイン
        var sizeLen = fontSize.length;
        var useImg = ovStr != "" && changeBtn.is("[src]");
        // 現在クッキー確認関数
        function nowCookie() {
            return $.cookie("digital_fontsize");
        }
        // 画像切替関数
        function imgChange(elm1, elm2, str1, str2) {
            elm1.attr("src", elm2.attr("src").replace(new RegExp("^(\.+)" + str1 + "(\\.[a-z]+)$"), "$1" + str2 + "$2"));
        }
        // マウスアウト関数
        function mouseOut() {
            for (var i = 0; i < sizeLen; i++) {
                if (nowCookie() != fontSize[i]) {
                    imgChange(changeBtn.eq(i), changeBtn.eq(i), ovStr, "");
                }
            }
        }
        // フォントサイズ設定関数
        function sizeChange() {

            changeArea.css({
                fontSize: nowCookie() + "%"
            });
        }
        // クッキー設定関数
        function cookieSet(index) {
            $.cookie("digital_fontsize", fontSize[index], {
                path: '/',
                expires: cookieExpires,
                domain: cookieDomain
            });
        }
        // 初期表示
        if (nowCookie()) {
            for (var i = 0; i < sizeLen; i++) {
                if (nowCookie() == fontSize[i]) {
                    sizeChange();
                    var elm = changeBtn.eq(i);
                    if (useImg) {
                        imgChange(elm, elm, "", ovStr);
                    }
                    elm.addClass(activeClass);
                    break;
                }
            }
        } else {
            cookieSet(defaultSize);
            sizeChange();
            var elm = changeBtn.eq(defaultSize);
            if (useImg) {
                imgChange(elm, elm, "", ovStr);
                imgChange($("<img>"), elm, "", ovStr);
            }
            elm.addClass(activeClass);
        }
        // ホバーイベント（画像タイプ）
        if (useImg) {
            changeBtn.each(function(i) {
                var self = $(this);
                self.hover(
                function() {
                    if (nowCookie() != fontSize[i]) {
                        imgChange(self, self, "", ovStr);
                    }
                },
                function() {
                    mouseOut();
                });
            });
        }
        // クリックイベント
        changeBtn.click(function() {
            var index = changeBtn.index(this);
            var self = $(this);
            cookieSet(index);
            sizeChange();
            if (useImg) {
                mouseOut();
            }
            if (!self.hasClass(activeClass)) {
                changeBtn.not(this).removeClass(activeClass);
                self.addClass(activeClass);
            }
        });
    }
})(jQuery);

/* 非フォーカスのテキスト表示
 * http://remysharp.com/2007/01/25/jquery-tutorial-text-box-hints/
==================== */
(function ($) {
    $.fn.hint = function (blurClass) {
        if (!blurClass) {
            blurClass = 'blur';
        }
        return this.each(function () {
            // get jQuery version of 'this'
            var $input = $(this),
            // capture the rest of the variable to allow for reuse
            title = $input.attr('title'),
            $form = $(this.form),
            $win = $(window);
            function remove() {
                if ($input.val() === title && $input.hasClass(blurClass)) {
                    $input.val('').removeClass(blurClass);
                }
            }
            // only apply logic if the element has the attribute
            if (title) {
                // on blur, set value to title attr if text is blank
                $input.blur(function () {
                    if (this.value === '') {
                        $input.val(title).addClass(blurClass);
                    }
                }).focus(remove).blur(); // now change all inputs to title
                // clear the pre-defined text when form is submitted
                $form.submit(remove);
                $win.unload(remove); // handles Firefox's autocomplete
            }
        });
    };
})(jQuery);
$(function() {
    // find all the input elements with title attributes
    $('input[title!=""]').hint();
});

/* 吹出しメニュー
==================== */
$(function() {
    // 初期設定
    var subUlWid = 0;
    var navNum = 0;
    var boxWid = 0;
    var fadeInTime = 0; /* fade in */
    var tallest = 0;
    var tallest2 = 0;
    var id_MoreMenu = 1; //もっと見る用認識ID
    var id_FontSize = 2; //フォントサイズボタン用認識ID
    var id_OptionTool = 3; //オプションツールボタン用認識ID
    var id_Setup = 4; //設定用認識ID
    var id_SubMenu = 5; //ジャンルメニュー用認識ID
    // グローバルナビ hover
    $("ul.GlobalNav li").not("ul.GlobalNav li ul li,ul.GlobalNav li.MoreMenu").hover(
    function () {
        clearBind();
        // add class
        $(this).addClass("hover");
        // div.SubNav の中の ulの幅を数字として取得
        boxWid = parseInt($("ul.GlobalNav li.hover div.SubNav ul").css("width"));
        // div.SubNav の表示設定
        var navWid = $("ul.GlobalNav li.hover").width(); // main naviの幅
        var boxLeft = (boxWid / 2) - (navWid / 2) ; // 位置計算（メインULの半分 サブメニューdivの半分）
        $("ul.GlobalNav li.hover div.SubNav").fadeIn(fadeInTime);
        $("ul.GlobalNav li.hover div.SubNav").css("left", - boxLeft); // 位置設定
    },
    function () {
        $(this).removeClass("hover");
        $("ul.GlobalNav li div.SubNav").hide();
    });
    //もっと見る click
    $("ul.GlobalNav li.MoreMenu").hover(
    function() {
        //範囲内hoverのとき
        //設定したイベントを初期化します。
        $("ul.GlobalNav li.MoreMenu").unbind("click");
        $("body").unbind("click");
        // もっと見る Click
        $("ul.GlobalNav li.MoreMenu").click(id_MoreMenu, function() {
            if ($(this).hasClass("click")) {
                $(this).removeClass("click");
                $("ul.GlobalNav li div.MoreMenuSubNav").hide();
            } else {
                clearBind();
                // add class
                $(this).addClass("click");
                // div.MoreMenuSubNav の表示設定
                $("ul.GlobalNav li.MoreMenu.click div.MoreMenuSubNav").fadeIn(fadeInTime);
                // dl の高さを揃える
                $("li.MoreMenu div.MoreMenuSubNav dl").each(function() {
                    if ($(this).height() > tallest) {
                        tallest = $(this).height();
                    }
                });
                $("ul.GlobalNav li.MoreMenu.click div.MoreMenuSubNav dl").height(tallest);
                // ul の高さを揃える
                $("ul.GlobalNav li.MoreMenu div.MoreMenuSubNav ul").not("ul.GlobalNav li.MoreMenu div.MoreMenuSubNav dl ul").each(function() {
                    var tallest = 0;
                    if ($(this).height() > tallest) {
                        tallest2 = $(this).height();
                    }
                });
                $("ul.GlobalNav li.MoreMenu.click div.MoreMenuSubNav ul").height(tallest2);
            }
        });
    },
    function() {
        //範囲外に出たとき
        $("body").click(function() {
            $("ul.GlobalNav li.MoreMenu").removeClass("click");
            $("ul.GlobalNav li div.MoreMenuSubNav").hide();
        });
    });
    //文字サイズ click
    $("ul.UserTool li.FontSize div").hover(
    function() {
        //範囲内hoverのとき
        //設定したイベントを初期化します。
        $("ul.UserTool li.FontSize div").unbind("click");
        $("body").unbind("click");
        // add class
        $(this).addClass("hover");
        $("ul.UserTool li.FontSize div").click(id_FontSize, function() {
            if ($(this).hasClass("click")) {
                $("ul.UserTool li.FontSize div").removeClass("click");
                $("ul.UserTool li.FontSize div div.SubNav").hide();
            } else {
                clearBind();
                $("ul.UserTool li.FontSize div").addClass("click");
                $("ul.UserTool li.FontSize div.hover div.SubNav").fadeIn(fadeInTime);
            }
        });
    },
    function() {
        //範囲外に出たとき
        // remove class
        $(this).removeClass("hover");
        $("body").click(function() {
            $("ul.UserTool li.FontSize div").removeClass("click");
            $("ul.UserTool li.FontSize div div.SubNav").hide();
        });
    });
    //オプションツール click
    $("ul.UserTool li.OptionTool div").hover(
    function() {
        //範囲内hoverのとき
        //設定したイベントを初期化します。
        $("ul.UserTool li.OptionTool div").unbind("click");
        $("body").unbind("click");
        // add class
        $(this).addClass("hover");
        $("ul.UserTool li.OptionTool div").click(id_OptionTool, function() {
            if ($(this).hasClass("click")) {
                $("ul.UserTool li.OptionTool div").removeClass("click");
                $("ul.UserTool li.OptionTool div div.SubNav").hide();
            } else {
                clearBind();
                $("ul.UserTool li.OptionTool div").addClass("click");
                $("ul.UserTool li.OptionTool div.hover div.SubNav").fadeIn(fadeInTime);
            }
        });
    },
    function() {
        //範囲外に出たとき
        // remove class
        $(this).removeClass("hover");
        $("body").click(function() {
            $("ul.UserTool li.OptionTool div").removeClass("click");
            $("ul.UserTool li.OptionTool div div.SubNav").hide();
        });
    });
    //設定 click
    $("ul.UserTool li.Setup div").hover(
    function() {
        //範囲内hoverのとき
        //設定したイベントを初期化します。
        $("ul.UserTool li.Setup div").unbind("click");
        $("body").unbind("click");
        // add class
        $(this).addClass("hover");
        $("ul.UserTool li.Setup div").click(id_Setup, function() {
            if ($(this).hasClass("click")) {
                $("ul.UserTool li.Setup div").removeClass("click");
                $("ul.UserTool li.Setup div div.SubNav").hide();
            } else {
                clearBind();
                $("ul.UserTool li.Setup div").addClass("click");
                $("ul.UserTool li.Setup div.hover div.SubNav").fadeIn(fadeInTime);
            }
        });
    },
    function() {
        //範囲外に出たとき
        // remove class
        $(this).removeClass("hover");
        $("body").click(function() {
            $("ul.UserTool li.Setup div").removeClass("click");
            $("ul.UserTool li.Setup div div.SubNav").hide();
        });
    });
    //ジャンルメニュー click
    $("div.SubMenu").hover(
    function() {
        //範囲内hoverのとき
        //設定したイベントを初期化します。
        $("div.SubMenu").unbind("click");
        $("body").unbind("click");
        // もっと見る Click
        $("div.SubMenu").click(id_SubMenu, function() {
            if ($(this).hasClass("click")) {
                $(this).removeClass("click");
                $("div.SubMenu div.SubMenuSubNav").hide();
            } else {
                var GenreBoxWid;
                var GenreDlNum = 0;
                var BigNum = 0;
                if (navigator.userAgent.indexOf("MSIE") != - 1) {
                    var yohaku = 7;
                } else {
                    var yohaku = 3;
                }
                clearBind();
                // add class
                $(this).addClass("click");
                // div.MoreMenuSubNav の表示設定
                $("div.SubMenu div.SubMenuSubNav").fadeIn(fadeInTime);
                // div.GenreMenuSubNav の中の dlの幅を数字として取得
                var dlWid = parseInt($("div.SubMenu div.SubMenuSubNav div.SubMenuSubNavInner .SubMenuSubNavBox dl").css("width"));
                // GenreMenuSubNavBoxの幅を指定
                // dlの1行の最大数確認
                $("div.SubMenu div.SubMenuSubNav div.SubMenuSubNavInner .SubMenuSubNavBox").each(function() {
                    GenreDlNum = $(this).find("dl").length;
                    if (GenreDlNum > BigNum) {
                        BigNum = GenreDlNum;
                    }
                    GenreDlNum = BigNum;
                });
                //dlの数を元にGenreMenuSubNavBoxの幅を指定
                if (GenreDlNum < 4 ) {
                    GenreBoxWid = dlWid * GenreDlNum + yohaku;
                } else {
                    GenreBoxWid = dlWid * 4 + yohaku;
                }
                $("div.SubMenu div.SubMenuSubNav div.SubMenuSubNavInner .SubMenuSubNavBox").width(GenreBoxWid);
                // dl の高さを揃える
                $("div.SubMenu div.SubMenuSubNav div.SubMenuSubNavInner .SubMenuSubNavBox").each(function() {
                    var tallest = 0;
                    $(this).find("dl").each(function() {
                        if ($(this).height() > tallest) {
                            tallest = $(this).height();
                        }
                    });
                    $(this).children("dl").height(tallest);
                });
            }
        });
    },
    function() {
        //範囲外に出たとき
        $("body").click(function() {
            $("div.SubMenu").removeClass("click");
            $("div.SubMenu div.SubMenuSubNav").hide();
        });
    });
    function clearBind() {
        /*
                    var id_MoreMenu　= 1;　//もっと見る用認識ID
                    var id_FontSize　= 2;　//フォントサイズボタン用認識ID
                    var id_OptionTool = 3;　//オプションツールボタン用認識ID
                    var id_Setup　= 4;　//設定用認識ID
                    var id_SubMenu　= 5;　//ジャンルメニュー用認識ID
                    */
        //ubindしないと同じボタンを押したときにOn,Offしない。
        $("ul.GlobalNav li.MoreMenu").unbind("click", id_MoreMenu);
        $("ul.UserTool li.FontSize div").unbind("click", id_FontSize);
        $("ul.UserTool li.OptionTool div").unbind("click", id_OptionTool);
        $("ul.UserTool li.Setup div").unbind("click", id_Setup);
        $("div.SubMenu").unbind("click", id_SubMenu);
        //ぜんぶみる
        $("ul.GlobalNav li.MoreMenu").removeClass("click");
        $("ul.GlobalNav li div.MoreMenuSubNav").hide();
        //フォントサイズ
        $("ul.UserTool li.FontSize div").removeClass("click");
        $("ul.UserTool li.FontSize div div.SubNav").hide();
        //オプションツール
        $("ul.UserTool li.OptionTool div").removeClass("click");
        $("ul.UserTool li.OptionTool div div.SubNav").hide();
        //設定
        $("ul.UserTool li.Setup div").removeClass("click");
        $("ul.UserTool li.Setup div div.SubNav").hide();
        //ジャンルメニュー
        $("div.SubMenu").removeClass("click");
        $("div.SubMenu div.SubMenuSubNav").removeClass("click");
        $("div.SubMenu div.SubMenuSubNav").hide();
    }
});

/* 天気・路線カスタマイズ（2014.12.16）
==================== */
$(function() {
    var $EnContentsList = $("#EnTopContentsInner .headInfoArea .EnContentsList");
    if (typeof weather_json !== 'undefined') {
        $EnWeather = $();
        $.ajax({
            url: weather_json,
            dataType: 'json',
            cache: false,
            timeout: 15000,
            success: function(data, textStatus, XMLHttpRequest) {
                //console.log(data, textStatus, XMLHttpRequest);

                if (data.weather && data.LastUpdDateTime) {
                    var timeDiff = (+new Date()) - (+new Date(data.LastUpdDateTime)); //mSec
                    if (timeDiff < 3600000) { // 時間差が3600秒未満なら表示する
                        $EnWeather
                            = $("<li>")
                                .addClass("EnWeather")
                                .append(
                                    $("<p>")
                                        .addClass("Image")
                                        .append(
                                            $("<img>")
                                                .attr({
                                                    'src': data.weather.image.source,
                                                    'alt': data.weather.image.source.alternate
                                                })
                                        )
                                    ,
                                    $("<span>")
                                        .addClass("EnWeatherArea")
                                        .text(data.weather.area.toUpperCase())
                                    ,
                                    $("<span>")
                                        .addClass("EnWeatherPop")
                                        .text("POP : " + data.weather.pop + "%")
                                );

                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //console.log(XMLHttpRequest, textStatus, errorThrown);
            },
            complete: function(XMLHttpRequest, textStatus) {
                //console.log(XMLHttpRequest, textStatus);
                $EnContentsList
                    .children(".Loading")
                        .remove()
                    .end()
                    .prepend($EnWeather);
            }
        });
    }
    if (typeof exchange_json !== 'undefined') {
        $EnExchange = $();
        $.ajax({
            url: exchange_json,
            dataType: 'json',
            cache: false,
            timeout: 15000,
            success: function(data, textStatus, XMLHttpRequest) {
                //console.log(data, textStatus, XMLHttpRequest);
                $USD = $();
                $EUR = $();
                if (data.exchange.length > 0 && data.LastUpdDateTime) {
                    var timeDiff = (+new Date()) - (+new Date(data.LastUpdDateTime)); //mSec
                    if (timeDiff < 86400000) { // 時間差が24時間未満なら表示する
                        $.each(data.exchange, function() {
                            if (this.name === "USDJPY") {
                                $USD
                                    = $("<li>")
                                        .addClass("USD-JPY")
                                        .append(
                                            $("<span>")
                                                .text("USD/JPY")
                                            ,
                                            this.rate
                                        )
                                ;
                            }
                            if (this.name === "EURJPY") {
                                $EUR
                                    = $("<li>")
                                        .addClass("EUR-JPY")
                                        .append(
                                            $("<span>")
                                                .text("EUR/JPY")
                                        ,
                                        this.rate
                                    )
                                ;
                            }
                        });

                        $EnExchange
                            = $("<li>")
                                .addClass("EnExchange")
                                .append(
                                    $("<ul>")
                                        .append($USD, $EUR)
                                )
                        ;
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //console.log(XMLHttpRequest, textStatus, errorThrown);
            },
            complete: function(XMLHttpRequest, textStatus) {
                //console.log(XMLHttpRequest, textStatus);
                $EnContentsList
                    .children(".Loading")
                        .remove()
                    .end()
                    .children(".EnTraffic")
                        .before($EnExchange);
            }
        });
    }

/*    var utility2015 = {
        init: function(theName) {
            //初期設定
            this.cookie = {};
            this.path =
            {
                'weather' : '/custom14/'
            }
            this.filetype = '.xml';
            this.data = {};

            //Cookie取得
            theName += "="; //　= を追加
            theCookie = document.cookie + ";"; //　検索時最終項目で-1になるのを防ぐ
            start = theCookie.indexOf(theName); //　指定されたセクション名を検索する
            if (start != - 1)
            {
                end = theCookie.indexOf(";", start); //　データを抜きだす
                this.cookie = unescape(theCookie.substring(start + theName.length, end));
            }

            //交通情報設定
            this.traffic_icon_official = [{
                "0": "公式情報無し",
                "1": "公式情報有り"
            }
            ];
            this.traffic_icon_twitter = [{
                "N": "ツイート無し",
                "M": "見合わせの可能性",
                "C": "遅延の可能性",
                "S": "再開の可能性"
            }
            ];
            this.traffic_msg = [{
                "N": "路線を設定",
                "S": "路線を再設定",
                "R": "リニューアルしました。"
            }
            ];
            //交通情報  jsonはasahi.comから取得。
            this.traffic_json = "/news/esi/traffic/custom_traffic.json" + '?' + Math.floor((new Date()).getTime() / (1000 * 60 * 5)); 5分
            //星座情報設定  jsonはasahi.comから取得。
            this.seiza_json = "/news/esi/uranai/custom_uranai.json" + '?' + Math.floor((new Date()).getTime() / (1000 * 60 * 5)); 5分

            //iref
            this.iref = [{
                "traffic": "?iref=comtop_ropponue_t"
            }, {
                "seiza": "?iref=comtop_ropponue_u"
            }
            ];

            return this;
        },
        showWeather: function(obj) {
            var obj_cookie;
            if (obj.cookie.length > 0 ) {
                obj_cookie = utility2015.string2Object(obj.cookie);
                //アサヒコムのクッキーはあるが、天気の情報が無い場合。
                if (!obj_cookie.weather) {
                    obj_cookie.weather = 'w_kanto_tokyo';
                }
                //weaterのURLを取得
                var url = obj.path.weather + obj_cookie.weather + obj.filetype + '?' + Math.floor((new Date()).getTime() / (1000 * 60 * 60)); 60分
                if (obj_cookie.weather) {
                    $.get(url,
                    function(xml, status)
                    {
                        if (status != 'success')
                            return;
                        var content = $('content', xml).text();
                        $("ul.UserCustomize li#UserCustomizeWeather").append(content);
                    });
                }
            } else {
                //アサヒコムにはじめてきた時
                var url = obj.path.weather + 'w_kanto_tokyo.xml' + '?' + Math.floor((new Date()).getTime() / (1000 * 60 * 60)); 60分
                $.get(url,
                function(xml, status)
                {
                    if (status != 'success')
                        return;
                    var content = $('content', xml).text();
                    $("ul.UserCustomize li#UserCustomizeWeather").append(content);
                });
            }
        },
        showTraffic: function(obj) {
            var obj_cookie;
            var data;
            var t_id, t_shortname, t_official, t_twitter, t_time, t_msg;
            if (obj.cookie.length > 0) {
                obj_cookie = utility2015.string2Object(obj.cookie);
            } else {
                obj_cookie = ['traffic'];
            }
            var jqXHR = $.getJSON(obj.traffic_json);
            jqXHR.success(function(json) {
                obj.data['traffic'] = json;
            });
            jqXHR.error(function() {
                obj.data['traffic'] = [];
            });
            jqXHR.complete(function() {
                if (obj_cookie.traffic) {
                    data = obj.data.traffic;
                    for (var i = 0, l = data.length; i < l; i++)
                    {
                        if (data[i]['i'] == obj_cookie.traffic)
                        {
                            t_id = data[i]['i']; //ID
                            t_shortname = data[i]['n']; //短縮名
                            t_official = data[i]['o']; //公式情報フラグ
                            t_twitter = data[i]['t']; //ツイッター情報
                            t_time = data[i]['j']; //時間
                            break;
                        }
                    }
                }
                if (t_id !== undefined ) {
                    content = '<p class="OfficialImage"><a href="/traffic/' + obj.iref[0]['traffic'] + '#line-' + t_id + '"><img src="/images/traffic/train_official_' + t_official + '.gif" width="15" height="22" alt="' + obj.traffic_icon_official[0][t_official] + '" /></a></p><p class="OfficialStatus"><a href="/traffic/' + obj.iref[0]['traffic'] + '#line-' + t_id + '">' + t_shortname + '<span class="Time"> (' + t_time + ')</span></a></p><p class="TwitterStatus"><a href="/traffic/' + obj.iref[0]['traffic'] + '#line-' + t_id + '"><img src="/images/train_twitter_' + t_twitter + '.gif" width="33" height="22" alt="' + obj.traffic_icon_twitter[0][t_twitter] + '" /></a></p>';
                } else {
                    if (obj_cookie.traffic) {
                        if (obj_cookie.traffic[0].indexOf('traffic') === 0 ) {
                            t_msg = obj.traffic_msg[0]['R'];
                        } else {
                            t_msg = obj.traffic_msg[0]['S'];
                        }
                    } else {
                        t_msg = obj.traffic_msg[0]['N'];
                    }
                    content = '<p class="OfficialImage"><a href="/traffic/' + obj.iref[0]['traffic'] + '"><img src="/images/traffic/train_official_0.gif" width="15" height="22" alt="公式情報無し" /></a></p><p class="OfficialStatus"><a href="/customize.html' + obj.iref[0]['traffic'] + '#TrafficCustomize">' + t_msg + '</a></p><p class="TwitterStatus"><a href="/traffic/' + obj.iref[0]['traffic'] + '"><img src="/images/train_twitter_N.gif" width="33" height="22" alt="ツイート無し" /></a></p>';
                }
                $("ul.UserCustomize li#UserCustomizeTraffic").append(content);
            });
        },
        showSeiza: function(obj) {
            var obj_cookie;
            var data, content;
            var seiza_icon, seiza_rank, seiza_kanji, seiza_url, seiza_status, seiza_info;

            if (obj.cookie.length > 0) {
                obj_cookie = utility2015.string2Object(obj.cookie);
            } else {
                obj_cookie = ['seiza'];
            }
            var jqXHR = $.getJSON(obj.seiza_json);
            jqXHR.success(function(json) {
                obj.data['seiza'] = json;
            });
            jqXHR.error(function() {
                obj.data['seiza'] = [];
            });
            jqXHR.complete(function() {
                if (obj_cookie.seiza) {
                    data = obj.data.seiza;
                    for (var i = 0, l = data.length; i < l; i++) {
                        if (data[i]['icon'] == obj_cookie.seiza[0]) {
                            seiza_icon = data[i]['icon'];
                            seiza_rank = data[i]['rank'];
                            seiza_kanji = data[i]['kanji'];
                            seiza_url = data[i]['url'];
                            seiza_status = obj_cookie.seiza[1];
                            break;
                        }
                    }
                }
                if (seiza_icon !== undefined ) {
                    if ( seiza_status == 0 || ( seiza_status == 2 && seiza_rank > 6)) {
                        seiza_info = seiza_kanji;
                    } else {
                        seiza_info = seiza_rank + '位';
                    }
                    content = '<p class="UranaiImage"><a href="' + seiza_url + obj.iref[1]['seiza'] + '"><img src="http://www.asahicom.jp/uranai/12seiza/images/icon_' + seiza_icon + '_top22.gif" width="22" height="22" alt="' + seiza_kanji + '"></a></p><p class="UranaiStatus"><a href="' + seiza_url + obj.iref[1]['seiza'] + '">' + seiza_info + '</a></p>';
                } else {
                    content = '<p class="UranaiImage"><a href="/customize.html' + obj.iref[1]['seiza'] + '#UranaiCustomize"><img src="http://www.asahicom.jp/uranai/12seiza/images/icon_Seiza_top22.gif" width="22" height="22" alt="12星座占い"></a></p><p class="UranaiStatus"><a href="/customize.html' + obj.iref[1]['seiza'] + '#UranaiCustomize">星座を設定</a></p>';
                }
                $("ul.UserCustomize li#UserCustomizeUranai").append(content);
            });
        },
        string2Object: function(string)
        {
            //String型からオブジェクト型にキャスト
            eval("var result = " + string);
            return result;
        }
    };
    if (($.fn.jquery === '1.8.3') && $("ul.UserCustomize").size() > 0) {
        $("ul.UserCustomize").prepend('<li id="UserCustomizeWeather"></li><li id="UserCustomizeUranai"></li><li id="UserCustomizeTraffic" class="TwitterTraffic"></li>');
        //実行（クッキーの内容含む）を取得
        util = utility2015.init('utilityData');
        var deferred = $.Deferred();
        deferred
        .then(function() {
            var d = $.Deferred();
            utility2015.showWeather(util);
            utility2015.showSeiza(util);
            utility2015.showTraffic(util);
            d.resolve();
        })
        .then(function() {
            $("ul.UserCustomize li.Set em").removeClass("Loading");
        });
        deferred.resolve();
    }*/
});

/* マイタウンカスタマイズ（トップ地域情報）
================================= */
/*$(function() {
    var MytownCustom = {
        init: function(theName) {
            //初期設定
            this.cookie = {};
            this.path =
            {
                'mytown': '/custom/'
            }
            this.filetype = '.xml';
            //Cookie取得
            theName += "="; // = を追加
            theCookie = document.cookie + ";"; //検索時最終項目で-1になるのを防ぐ
            start = theCookie.indexOf(theName); //指定されたセクション名を検索する
            if (start != - 1)
            {
                end = theCookie.indexOf(";", start); //データを抜きだす
                this.cookie = unescape(theCookie.substring(start + theName.length, end));
            }
            return this;
        },
        showMytown: function(obj) {

            if (!obj.cookie.length)
                return;

            obj_cookie = MytownCustom.string2Object(obj.cookie);
            var src = obj_cookie.mytown;
            if (!!src)
            {
                var url = this.path.mytown + src + this.filetype + '?' + Math.floor((new Date()).getTime() / (1000 * 60 * 10)); 10分
                jQuery.get(url,
                function(xml, status)
                {
                    if (status != 'success')
                        return;
                    var content = jQuery('content', xml).text();
                    jQuery('#MyTown').html(content);
                });
            }
        },
        string2Object: function(string)
        {
            //String型からオブジェクト型にキャスト
            eval("var result = " + string);
            return result;
        }
    };
    if ($('#MyTown').size() > 0) {
        //実行（クッキーの内容含む）を取得
        util = MytownCustom.init('utilityData');
        //エリアを表示
        MytownCustom.showMytown(util);
    }
});*/

/* クラスの追加
==================== */
//リロードボタン
$(function() {
    $("ul.UserTool li.Reload div").hover(
    function() {
        $(this).addClass("hover");
    },
    function() {
        $(this).removeClass("hover");
    });
});
//ホームに戻るボタン
$(function() {
    $("ul.UserTool li.ToHome div").hover(
    function() {
        $(this).addClass("hover");
    },
    function() {
        $(this).removeClass("hover");
    });
});

/* 右ナビ写真枠動画枠
==================== */
//写真枠
var vPhotoSlideAnimFlag = false;
var vPhotoSlideNumNow = 0;
var vPhotoSlideNumMax = 0;
var vPhotoSlideSlideArray = new Array();
var vPhotoSlideHTML = '';
$(document).ready(function() {
    vPhotoSlideNumMax = $('div.PhotosSlide .PhotosList li').length;
    for (var i = 0; i < $('div.PhotosSlide .PhotosList li').length; i++) {
        vPhotoSlideSlideArray[i] = String( $('div.PhotosSlide .PhotosList li:eq(' + i + ')').html() );
    };
    vPhotoSlideHTML = '';
    vPhotoSlideHTML += ('<li>' + vPhotoSlideSlideArray[vPhotoSlideNumMax - 2] + '</li>');
    vPhotoSlideHTML += ('<li>' + vPhotoSlideSlideArray[vPhotoSlideNumMax - 1] + '</li>');
    for (var i = 0; i < vPhotoSlideNumMax; i++) {
        vPhotoSlideHTML += ('<li>' + vPhotoSlideSlideArray[i] + '</li>');
    }
    vPhotoSlideHTML += ('<li>' + vPhotoSlideSlideArray[0] + '</li>');
    vPhotoSlideHTML += ('<li>' + vPhotoSlideSlideArray[1] + '</li>');
    $('div.PhotosSlide .PhotosList').html(vPhotoSlideHTML);
    $('div.PhotosSlide ul.PhotosList').css({
        width: ( ((vPhotoSlideNumMax + 4) * 130) )
    });
    vPhotoSlideNumMax = - (vPhotoSlideNumMax);
    vPhotoSlideNumNow = - 2;
    $('div.PhotosSlide .PhotosList').css({
        left: ((vPhotoSlideNumNow) * 130)
    });
    $('div.PhotosSlide .PhotosBtnL a').click(function() {
        if (vPhotoSlideAnimFlag == false) {
            vPhotoSlideAnimFlag = true;
            vPhotoSlideNumNow++;
            if (vPhotoSlideNumNow > 0) {
                vPhotoSlideNumNow = vPhotoSlideNumMax;
                $('div.PhotosSlide .PhotosList').css({
                    left: ((vPhotoSlideNumNow) * 130)
                });
                vPhotoSlideNumNow++;
            }
            $('div.PhotosSlide .PhotosList').animate({
                left: (vPhotoSlideNumNow * 130)
            }, 350, function() {
                if (vPhotoSlideNumNow >= 0) {
                    vPhotoSlideNumNow = vPhotoSlideNumMax;
                    $('div.PhotosSlide .PhotosList').css({
                        left: ((vPhotoSlideNumNow) * 130)
                    });
                }
                vPhotoSlideAnimFlag = false;
            });
        }
    });
    $('div.PhotosSlide .PhotosBtnR a').click(function() {
        if (vPhotoSlideAnimFlag == false) {
            vPhotoSlideAnimFlag = true;
            vPhotoSlideNumNow--;
            if (vPhotoSlideNumNow < vPhotoSlideNumMax) {
                vPhotoSlideNumNow = 0;
                $('div.PhotosSlide .PhotosList').css({
                    left: ((vPhotoSlideNumNow) * 130)
                });
                vPhotoSlideNumNow =- 1;
            }
            $('div.PhotosSlide .PhotosList').animate({
                left: (vPhotoSlideNumNow * 130)
            }, 350, function() {
                if (vPhotoSlideNumNow <= vPhotoSlideNumMax) {
                    vPhotoSlideNumNow = 0;
                    $('div.PhotosSlide .PhotosList').css({
                        left: ((vPhotoSlideNumNow) * 130)
                    });
                }
                vPhotoSlideAnimFlag = false;
            });
        }
    });
    $('div.PhotosSlide').css('visibility', 'visible');
});
//動画枠
var vVideoSlideAnimFlag = false;
var vVideoSlideNumNow = 0;
var vVideoSlideNumMax = 0;
var vVideoSlideSlideArray = new Array();
var vVideoSlideHTML = '';
$(document).ready(function() {
    vVideoSlideNumMax = $('div.VideosSlide .VideosList li').length;
    for (var i = 0; i < $('div.VideosSlide .VideosList li').length; i++) {
        vVideoSlideSlideArray[i] = String( $('div.VideosSlide .VideosList li:eq(' + i + ')').html() );
    };
    vVideoSlideHTML = '';
    vVideoSlideHTML += ('<li>' + vVideoSlideSlideArray[vVideoSlideNumMax - 2] + '</li>');
    vVideoSlideHTML += ('<li>' + vVideoSlideSlideArray[vVideoSlideNumMax - 1] + '</li>');
    for (var i = 0; i < vVideoSlideNumMax; i++) {
        vVideoSlideHTML += ('<li>' + vVideoSlideSlideArray[i] + '</li>');
    };
    vVideoSlideHTML += ('<li>' + vVideoSlideSlideArray[0] + '</li>');
    vVideoSlideHTML += ('<li>' + vVideoSlideSlideArray[1] + '</li>');
    $('div.VideosSlide .VideosList').html(vVideoSlideHTML);
    $('div.VideosSlide ul.VideosList').css({
        width: ( ((vVideoSlideNumMax + 4) * 130) )
    });
    vVideoSlideNumMax = - (vVideoSlideNumMax);
    vVideoSlideNumNow = - 2;
    $('div.VideosSlide .VideosList').css({
        left: ((vVideoSlideNumNow) * 130)
    });
    $('div.VideosSlide .VideosBtnL a').click(function() {
        if (vVideoSlideAnimFlag == false) {
            vVideoSlideAnimFlag = true;
            vVideoSlideNumNow++;
            if (vVideoSlideNumNow > 0) {
                vVideoSlideNumNow = vVideoSlideNumMax;
                $('div.VideosSlide .VideosList').css({
                    left: ((vVideoSlideNumNow) * 130)
                });
                vVideoSlideNumNow++;
            }
            $('div.VideosSlide .VideosList').animate({
                left: (vVideoSlideNumNow * 130)
            }, 350, function() {
                if (vVideoSlideNumNow >= 0) {
                    vVideoSlideNumNow = vVideoSlideNumMax;
                    $('div.VideosSlide .VideosList').css({
                        left: ((vVideoSlideNumNow) * 130)
                    });
                }
                vVideoSlideAnimFlag = false;
            });
        }
    });
    $('div.VideosSlide .VideosBtnR a').click(function() {
        if (vVideoSlideAnimFlag == false) {
            vVideoSlideAnimFlag = true;
            vVideoSlideNumNow--;
            if (vVideoSlideNumNow < vVideoSlideNumMax) {
                vVideoSlideNumNow = 0;
                $('div.VideosSlide .VideosList').css({
                    left: ((vVideoSlideNumNow) * 130)
                });
                vVideoSlideNumNow =- 1;
            }
            $('div.VideosSlide .VideosList').animate({
                left: (vVideoSlideNumNow * 130)
            }, 350, function() {
                if (vVideoSlideNumNow <= vVideoSlideNumMax) {
                    vVideoSlideNumNow = 0;
                    $('div.VideosSlide .VideosList').css({
                        left: ((vVideoSlideNumNow) * 130)
                    });
                }
                vVideoSlideAnimFlag = false;
            });
        }
    });
    $('div.VideosSlide').css('visibility', 'visible');
});

/* 広告カウント画像の非表示
==================== */
$(function() {
    $("div.Ad").each(function() {
        $("img", this).each(function() {
            w = $(this).width();
            if ( w === 1) {
                $(this).css("display", "none");
            }
        });
    });
});

/* 一記事印刷
==================== */
function openPrintArticle() {
    myWin = window.open(window.location.href, "PrintArticle", "toolbar=0,location=0,directories=0,status=0,scrollbars=1,width=800,resizable=yes");
    window.blur();
}
if (window.name == "PrintArticle") {
    var potaufeuAssetUrl = '';
    switch (window.location.host) {
        case "www.asahi.com":
        case "ajw.potaufeu.asahi.com":
        case "ajw-r1.potaufeu.asahi.com":
        case "ajw-r2.potaufeu.asahi.com":
            potaufeuAssetUrl = 'https://public.potaufeu.asahi.com/';
            break;
        case "wwwcdn.asahi.com":
        case "ajw-d.potaufeu.asahi.com":
            potaufeuAssetUrl = 'https://public-d.potaufeu.asahi.com/';
            break;
        default:
            potaufeuAssetUrl = 'https://public.potaufeu.asahi.com/';
            break;
    }

    document.write('<link rel="stylesheet" type="text/css" href="' + potaufeuAssetUrl + 'ajw/css/en-asahi-print2016.css" media="screen,print" />');

    if ( navigator.userAgent.indexOf('Safari/') != - 1 ) {
        setTimeout(function() {
            print();
        }, 1500);
        window.focus();
    } else {
        window.focus();
        setTimeout(function() {
            print();
        }, 2500);
    }
}

/* 一記事メール
==================== */
$(function() {
    var SbjTxt = "Send this URL by email"; //件名を指定（※日本語だとユーザ環境により文字化けする）
    $('#ArticleTools #UtilityTools li.Mail a').attr('href', 'mailto:?subject=' + SbjTxt + '&body=' + encodeURI(window.location.href));
});

/* 一覧サムネイル枠キャプション
==================== */
$(function() {
    $('.ListSideImage .Image').mouseenter(function(e) {
        $(this).children('a').children('span:not(:animated)').fadeIn(10);
    }).mouseleave(function(e) {
        $(this).children('a').children('span:not(:animated)').fadeOut(10);
    });
});
$(function() {
    $('.ListSideImage .HeadlineImage').mouseenter(function(e) {
        $(this).children('a').children('span:not(:animated)').fadeIn(10);
    }).mouseleave(function(e) {
        $(this).children('a').children('span:not(:animated)').fadeOut(10);
    });
});

/* 一記事フォローリンク枠アコーディオン
==================== */
$(function() {
    $("#ShimenTools a").hover(
    function() {
        $(this).addClass("hover");
    },
    function() {
        $(this).removeClass("hover");
    });
    $("#ShimenTools a").click(function() {
        var menu_id = '#ArticleTools';
        var menu_status = $(menu_id + ' div:visible').html();
        var select_menu = $(this).attr('id').replace('Btn', '');
        var prev_menu = (menu_status) ? $(menu_id + ' a.Selected').attr('id').replace('Btn', '') : "";
        $('#ShimenTools .Selected').removeClass('Selected');
        if ((select_menu == prev_menu) && menu_status) {
            $(menu_id + ' div:visible').slideUp('normal');
        } else {
            (menu_status) ? $(menu_id + ' div:visible').slideUp('normal', function() {
                $('#' + select_menu + 'Nav').slideDown('normal');
            }) : $('#' + select_menu + 'Nav').slideDown('normal');
            $(this).addClass("Selected");
        }
    });
});

/* 一記事スクラップアラート
==================== */
/*function scrap_add_article( aid ) {
    $("#UtilityTools li.Scrap span").addClass("Loading");
    // ajax通信
    $.ajax({
        url: "/scrapbook/scrapbook_add.html",
        type: "post",
        dataType: "text",
        // データ形式はTEXTを指定
        data: {
            // リクエストパラメータを定義
            "aid": aid
        },
        cache: false,
        // キャッシュを使用
        success: function( res ) {
            // データ取得に成功した場合の処理を定義
            if ( res == "1" ) {
                $("#ShimenListNav,#ShimenBackNav").hide();
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                $("#ScrapAlert").slideDown();
                $("#ScrapAlert dd p").html("この記事をスクラップしました");
                $("#ScrapAlert").delay(5000).slideUp();
            } else if ( res == "2" ) {
                $("#ShimenListNav,#ShimenBackNav").hide();
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                $("#ScrapAlert").slideDown();
                $("#ScrapAlert dd p").html("この記事はスクラップ済です");
                $("#ScrapAlert").delay(5000).slideUp();
            } else if ( res == "8" ) {
                $("#ShimenListNav,#ShimenBackNav").hide();
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                $("#ScrapAlert").slideDown();
                $("#ScrapAlert dd p").html("スクラップのご利用には朝日新聞デジタルの購読申し込みが必要です");
                $("#ScrapAlert").delay(5000).slideUp();
            } else if ( res == "9" ) {
                $("#ShimenListNav,#ShimenBackNav").hide();
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                $("#ScrapAlert").slideDown();
                $("#ScrapAlert dd p").html("スクラップはただいまご利用いただけません");
                $("#ScrapAlert").delay(5000).slideUp();
            } else if ( res == "0" ) {
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                alert( res );
            } else {
                $("#UtilityTools li.Scrap span").removeClass("Loading");
                alert( res );
            }
        }
    });
}*/

/* 一記事朝刊紙面一覧
==================== */
var men1msnBoxCol = 4;
$(function() {
    if ($("#ShimenTools").children().hasClass("ShimenListBtn")) {
        var men1msnBox = ".ShimenListNav";
        var men1msnBoxLiSizeAll = $(men1msnBox).find("li").size();
        men1msnBoxLiSize = men1msnBoxLiSizeAll / men1msnBoxCol;
        men1msnBoxLiSize = Math.ceil(men1msnBoxLiSize);
        var menList = [];
        /* liの要素を抜く */
        $(".ShimenListNav ul li").each(function(i) {
            menList[i] = $(this).html();
        });
        for ( k = 0; k < men1msnBoxCol; k = k + 1 ) {
            var menULclassNum = k + 1;
            var appendDL = '<ul class="ShimenList' + menULclassNum + '"></ul>';
            $(".ShimenListNav").append(appendDL);
            var appendUL = '.ShimenListNav .ShimenList' + menULclassNum;
            var liMin = men1msnBoxLiSize * k;
            var liMax = men1msnBoxLiSize;
            if (k != (men1msnBoxCol - 1)) {
                liMax = k * men1msnBoxLiSize + men1msnBoxLiSize;
            } else {
                liMax = men1msnBoxLiSizeAll;
            }
            var i;
            for ( i = liMin; i < liMax; i = i + 1 ) {
                $(appendUL).append('<li>' + menList[i] + '</li>');
            }
        }
        /* オリジナルを隠す */
        $(".ShimenListSource").css("display", "none");
    }
});

/* タブ切り替え速報ニュース
==================== */
$(function() {
    $("ul.GenreTab li.Headlines a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Headlines-List").show();
        $("#National-List,#Politics-List,#Business-List,#International-List,#Techscience-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.National a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#National-List").show();
        $("#Headlines-List,#Politics-List,#Business-List,#International-List,#Techscience-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.Politics a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Politics-List").show();
        $("#Headlines-List,#National-List,#Business-List,#International-List,#Techscience-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.Business a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Business-List").show();
        $("#Headlines-List,#National-List,#Politics-List,#International-List,#Techscience-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.International a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#International-List").show();
        $("#Headlines-List,#National-List,#Politics-List,#Business-List,#Techscience-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.Techscience a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Techscience-List").show();
        $("#Headlines-List,#National-List,#Politics-List,#Business-List,#International-List,#Sports-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.Sports a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Sports-List").show();
        $("#Headlines-List,#National-List,#Politics-List,#Business-List,#International-List,#Techscience-List,#Culture-List").hide();
    });
    $("ul.GenreTab li.Culture a").click(function() {
        $(this).addClass("Selected");
        $('ul.GenreTab li a').not(this).removeClass("Selected");
        $("#Culture-List").show();
        $("#Headlines-List,#National-List,#Politics-List,#Business-List,#International-List,#Techscience-List,#Sports-List").hide();
    });
});

/* 汎用モジュール-タブ
==================== */
$(function() {
    $("div.TabMod ul.TabCol li a").bind('click', function() {
        if ($(this).attr("href").indexOf("javascript:void(0)") != - 1) {
            $(this).parent().parent().find('a').removeClass('Selected');
            $(this).parent().parent().parent().find('div.TabPanel:visible').hide();
            $(this).parent().parent().parent().find('div.TabPanel').eq($(this).parent().index()).show();
            $(this).addClass('Selected');
        }
    });
});

/* スマホ・マックの判別及びクラスの追加
 * http://rafael.adm.br/css_browser_selector#contributors
==================== */
function css_browser_selector(u) {
    var ua = u.toLowerCase(), is = function(t) {
        return ua.indexOf(t)>-1
    }, m = 'mobile', h = document.documentElement, b = [is('android') ? is(m) ? 'android_smp' : 'android_tab': is('iphone') ? ' iphone': is('ipad') ? ' ipad': is('mac') ? 'mac': is('darwin') ? 'mac': ' '];
    c = b.join(' ');
    h.
    className += ' ' + c;
    return c;
};
css_browser_selector(navigator.userAgent);
/*console.log(css_browser_selector(navigator.userAgent));
console.log(navigator.userAgent);*/

/* アプリ（朝デジ・高校野球）iPad/iPhoneウェブビュー時に購入導線を非表示 150703
==================== */
if (navigator.userAgent.indexOf('TIDAViewer-I')!=-1 || navigator.userAgent.indexOf('TIDAViewer-i')!=-1 || navigator.userAgent.indexOf('tokutenViewer-I')!=-1 || navigator.userAgent.indexOf('tokutenViewer-i')!=-1 || navigator.userAgent.indexOf('Nadia-I')!=-1 || navigator.userAgent.indexOf('Nadia-K')!=-1 || navigator.userAgent.indexOf('Nadia-i')!=-1 || navigator.userAgent.indexOf('Nadia-k')!=-1 || navigator.userAgent.indexOf('KOYA_APP-i')!=-1 || navigator.userAgent.indexOf('KOYA_APP-I')!=-1) {
    $(function() {
        $('.NoApp,#Sub .Tools .SignupNav,.UserNav .Join a.Logout').remove();
    });
};

/* アプリiOS・Android・Kindleウェブビュー表示時のコンテンツ非表示 150305
==================== */
if (navigator.userAgent.indexOf('Nadia-I')!=-1 || navigator.userAgent.indexOf('Nadia-i')!=-1) {
    $(function() {
        $('.NoAppAllUa,.NoAppIos').css('cssText', 'display: none !important;');
    });
};
if (navigator.userAgent.indexOf('Nadia-A')!=-1 || navigator.userAgent.indexOf('Nadia-a')!=-1) {
    $(function() {
        $('.NoAppAllUa,.NoAppAndroid').css('cssText', 'display: none !important;');
    });
};
if (navigator.userAgent.indexOf('Nadia-K')!=-1 || navigator.userAgent.indexOf('Nadia-k')!=-1) {
    $(function() {
        $('.NoAppAllUa,.NoAppKindle').css('cssText', 'display: none !important;');
    });
};


/* フィーチャーボックス（パラパラ/５枚切り替え）
last modified -2014-10-14
裏表5割ランダム（トップ含む）
==================== */
var vFeaturePhotoSlideAnimFlag = false;
var vFeaturePhotoSlideNumNow = 0;
var vFeaturePhotoSlideNumMax = 0;
var vFeaturePhotoSlideSlideArray = new Array();
var vFboxSlideHTML = '';
var vFboxAnimInt;
var vFboxAnimCount = 0;
var vFboxImgObjAry = new Array();
var vFeatureChange = 0;
function xFboxChange(_ClickBtn) {
    var _FboxNewHTML = '';
    switch (_ClickBtn) {
    case 'left':
        $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + vFboxAnimCount + ')').html('');
        if (vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount] != 'none') {
            var _New_aTag = $('<a />');
            _New_aTag.attr('href', vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['href']);
            _New_aTag.append(vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['image']);
            _New_aTag.append('<span>' + vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['text'] + '</span>');
            $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + vFboxAnimCount + ')').append(_New_aTag);
        };
        vFboxAnimCount++;
        if (vFboxAnimCount == 5) {
            clearInterval(vFboxAnimInt);
            vFeaturePhotoSlideAnimFlag = false;
        };
        break;
    case 'right':
        $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + vFboxAnimCount + ')').html('');
        if (vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount] != 'none') {
            var _New_aTag = $('<a />');
            _New_aTag.attr('href', vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['href']);
            _New_aTag.append(vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['image']);
            _New_aTag.append('<span>' + vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][vFboxAnimCount]['text'] + '</span>');
            $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + vFboxAnimCount + ')').append(_New_aTag);
        };
        vFboxAnimCount--;
        if (vFboxAnimCount == - 1) {
            clearInterval(vFboxAnimInt);
            vFeaturePhotoSlideAnimFlag = false;
        };
        break;
    };
};
if (GetBrowser() != 'IE') {
    document.write("<style>.FeaturePhotoSlide{visibility:visible !important;}</style>");
}
if (location.pathname == "/") {
    //vFeatureChange = -1; //トップはランダムなし
}
var r = Math.floor(Math.random() * 2); //0-1
if ( r > vFeatureChange ) {
    document.write("<style>.FeaturePhotoSlide li:nth-child(n+6) {display:none;}</style>");
    vFeaturePhotoSlideNumNow = 0;
} else {
    document.write("<style>.FeaturePhotoSlide li:nth-child(-n+5) {display:none;}</style>");
    vFeaturePhotoSlideNumNow = 1;
}
$(document).ready(function() {
    if ($('#FeatureBox').get(0)) {
        $('#FeatureBox .FeaturePhotoSlide ul.FeaturePhotoList').css('width', '650px');
        var myFboxLeng = 5 * Math.ceil($('div.FeaturePhotoSlide .FeaturePhotoList li').length / 5);
        vFeaturePhotoSlideNumMax = Math.ceil($('div.FeaturePhotoSlide .FeaturePhotoList li').length / 5);
        if (vFeaturePhotoSlideNumMax == 1) {
            $('div.FeaturePhotoSlide .FeaturePhotoBtnL').css('display', 'none');
            $('div.FeaturePhotoSlide .FeaturePhotoBtnR').css('display', 'none');
        };
        var vFboxImgNameAry = new Array();
        for (i = 0; i < $('div.FeaturePhotoSlide .FeaturePhotoList li').length; i++) {
            vFboxImgNameAry.push( $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + i + ') img').attr('src') );
        };
        for (i = 0; i < vFboxImgNameAry.length; i++) {
            vFboxImgObjAry[i] = new Image();
            vFboxImgObjAry[i].src = vFboxImgNameAry[i];
        };
        var _myN = - 1;
        for (var i = 0; i < myFboxLeng; i++) {
            if ((i%5) == 0) {
                _myN++;
                vFeaturePhotoSlideSlideArray[_myN] = [];
                var _Fobj = {};
                _Fobj['href'] = $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + i + ') a').attr('href');
                _Fobj['image'] = vFboxImgObjAry[i];
                _Fobj['text'] = $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + i + ') span').html();
                vFeaturePhotoSlideSlideArray[_myN].push( _Fobj );
            } else {
                if ( i > $('div.FeaturePhotoSlide .FeaturePhotoList li').length - 1 ) {
                    vFeaturePhotoSlideSlideArray[_myN].push('none');
                } else {
                    var _Fobj = {};
                    _Fobj['href'] = $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + i + ') a').attr('href');
                    _Fobj['image'] = vFboxImgObjAry[i];
                    _Fobj['text'] = $('div.FeaturePhotoSlide .FeaturePhotoList li:eq(' + i + ') span').html();
                    vFeaturePhotoSlideSlideArray[_myN].push( _Fobj );
                };
            };
        };
        $('div.FeaturePhotoSlide .FeaturePhotoList').html('');
        for (i = 0; i < 5; i++) {
            var _New_aTag = $('<a />');
            _New_aTag.attr('href', vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][i]['href']);
            _New_aTag.append(vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][i]['image']);
            _New_aTag.append('<span>' + vFeaturePhotoSlideSlideArray[vFeaturePhotoSlideNumNow][i]['text'] + '</span>');
            var _New_li = $('<li />');
            _New_li.append(_New_aTag);
            $('div.FeaturePhotoSlide .FeaturePhotoList').append(_New_li);
        };
        $('div.FeaturePhotoSlide .FeaturePhotoList li').show();

        $('div.FeaturePhotoSlide .FeaturePhotoBtnL a').click(function() {
            $('div.FeaturePhotoSlide .FeaturePhotoList li:nth-child(n+6)').hide();
            if (vFeaturePhotoSlideAnimFlag == false) {
                vFeaturePhotoSlideAnimFlag = true;
                if (vFeaturePhotoSlideNumNow > 0) {
                    vFeaturePhotoSlideNumNow--;
                } else {
                    vFeaturePhotoSlideNumNow = vFeaturePhotoSlideNumMax - 1;
                };
                vFboxAnimCount = 0;
                vFboxAnimInt = setInterval('xFboxChange("left")', 80);
            };
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnR a').click(function() {
            $('div.FeaturePhotoSlide .FeaturePhotoList li:nth-child(n+6)').hide();
            if (vFeaturePhotoSlideAnimFlag == false) {
                vFeaturePhotoSlideAnimFlag = true;
                if (vFeaturePhotoSlideNumNow < vFeaturePhotoSlideNumMax - 1) {
                    vFeaturePhotoSlideNumNow++;
                } else {
                    vFeaturePhotoSlideNumNow = 0;
                };
                vFboxAnimCount = 4;
                vFboxAnimInt = setInterval('xFboxChange("right")', 80);
            };
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnL a').mousedown(function() {
            $(this).fadeTo(0, 0.5);
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnL a').mouseup(function() {
            $(this).fadeTo(0, 1);
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnL a').mouseleave(function() {
            $(this).fadeTo(0, 1);
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnR a').mousedown(function() {
            $(this).fadeTo(0, 0.5);
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnR a').mouseup(function() {
            $(this).fadeTo(0, 1);
        });
        $('div.FeaturePhotoSlide .FeaturePhotoBtnR a').mouseleave(function() {
            $(this).fadeTo(0, 1);
        });
        $('div.FeaturePhotoSlide').css('visibility', 'visible');
    };
});



/* 災害情報枠アコーディオン
==================== */
$(function() {
    $("#InsideUrgent .UrgentNav .SlideBtn").click(function() {
        if ($("#InsideUrgent .UrgentSlide").css("display") == "block") {
            $("#InsideUrgent .UrgentNav .SlideBtn span b").removeClass("Click");
            $("#InsideUrgent .UrgentNav .SlideBtn span b").html("詳しく");
            $("#InsideUrgent .UrgentSlide").slideUp();
        } else {
            $("#InsideUrgent .UrgentNav .SlideBtn b").addClass("Click");
            $("#InsideUrgent .UrgentNav .SlideBtn b").html("閉じる");
            $("#InsideUrgent .UrgentSlide").slideDown();
        }
    });
});

/* 紙面枠アコーディオン
==================== */
$(function() {
    $("#ShimenSwitch a").hover(
    function() {
        $(this).addClass("hover");
    },
    function() {
        $(this).removeClass("hover");
    });
    $("#ShimenSwitch a").click(function() {
        var mainss_menu_id = '#ShimenPageTools';
        var mainss_menu_status = $(mainss_menu_id + ' div:visible').html();
        var mainss_select_menu = $(this).attr('id').replace('Btn', '');
        var mainss_prev_menu = (mainss_menu_status) ? $(mainss_menu_id + ' a.Selected').attr('id').replace('Btn', '') : "";
        $('#ShimenSwitch .Selected').removeClass('Selected');
        if ((mainss_select_menu == mainss_prev_menu) && mainss_menu_status) {
            $(mainss_menu_id + ' div:visible').slideUp('normal');
        } else {
            (mainss_menu_status) ? $(mainss_menu_id + ' div:visible').slideUp('normal', function() {
                $('#' + mainss_select_menu + 'Nav').slideDown('normal');
            }) : $('#' + mainss_select_menu + 'Nav').slideDown('normal');
            $(this).addClass("Selected");
        }
    });
});

/* 右ナビ紙面枠アコーディオン
==================== */
$(function() {
    $("#RnaviShimenSwitch a").hover(
    function() {
        $(this).addClass("hover");
    },
    function() {
        $(this).removeClass("hover");
    });
    $("#RnaviShimenSwitch a").click(function() {
        var subss_menu_id = '#RnaviShimenPageTools';
        var subss_menu_status = $(subss_menu_id + ' div:visible').html();
        var subss_select_menu = $(this).attr('id').replace('Btn', '');
        var subss_prev_menu = (subss_menu_status) ? $(subss_menu_id + ' a.Selected').attr('id').replace('Btn', '') : "";
        $('#RnaviShimenSwitch .Selected').removeClass('Selected');
        if ((subss_select_menu == subss_prev_menu) && subss_menu_status) {
            $(subss_menu_id + ' div:visible').slideUp('normal');
        } else {
            (subss_menu_status) ? $(subss_menu_id + ' div:visible').slideUp('normal', function() {
                $('#' + subss_select_menu + 'Nav').slideDown('normal');
            }) : $('#' + subss_select_menu + 'Nav').slideDown('normal');
            $(this).addClass("Selected");
        }
    });
});

/* 一記事拡大写真
==================== */
/*$(function(){
  $("#GalleryInfo a").click(function(){
    if($(".GalleryMod .Image .Navi").css("display")=="block"){
      $(".GalleryMod .Image .Navi").slideUp();
      $(".GalleryMod .Image .Caption").slideUp();
    $("#GalleryInfo a").attr("class","GalleryInfoOpen");
    }else{
      $(".GalleryMod .Image .Navi").slideDown("fast");
      $(".GalleryMod .Image .Caption").slideDown("fast");
    $("#GalleryInfo a").attr("class","GalleryInfoClose");
    }
  });
});*/
$(function() {
    $("#GalleryInfoBtn a").click(function() {
        if ($(".GalleryMod .Image .Navi").is(":hidden")) {
            $(".GalleryMod .Image .Navi").slideDown("fast");
            $(".GalleryMod .Image .Caption").slideDown("fast");
            /*$("#GalleryInfoBtn a").attr("class","GalleryInfoBtnClose");*/
            $("#GalleryInfoBtn a").html("Hide Details");
        } else {
            $(".GalleryMod .Image .Navi").slideUp();
            $(".GalleryMod .Image .Caption").slideUp();
            /*$("#GalleryInfoBtn a").attr("class","GalleryInfoBtnOpen");*/
            $("#GalleryInfoBtn a").html("Show Details");
        }
    });
});

/**************************************************************************
/* jQuery SP View Mode  cmn02
 * 使用箇所：PC閲覧モード用クッキーセット
 * 　　　　　sp版->pc版へページ遷移するときにdigital_sp_pcmodeをONする。
 * relate : cmn01
 * update : 2013.04.15
 **************************************************************************/
var spViewMode = {
    // 現在クッキー確認関数
    nowCookie: function() {
        return $.cookie("digital_sp_pcmode");
    },
    // クッキー設定関数
    cookieSet: function() {
        var cookieExpires = 7;
        var cookieDomain = "asahi.com";
        $.cookie("digital_sp_pcmode", "ON", {
            path: '/',
            expires: cookieExpires,
            domain: cookieDomain
        });
    },
    cookieClear: function() {
        var date = new Date();
        var cookieExpires = 0;
        var cookieDomain = "asahi.com";
        $.cookie("digital_sp_pcmode", "", {
            path: '/',
            expires: cookieExpires,
            domain: cookieDomain
        });
    }
};

/* 2014.11.18 */
/* PC版からスマートフォン版へのリンク */
$(function() {
    $(".SelectButtonSMP").click(function() {
        spViewMode.cookieClear();
        var _SelectButtonSMP = location.pathname;
        var iref = "?iref=comtop_footerue";
        if (location.pathname.charAt(0) != "/")
            _SelectButtonSMP = "/" + _SelectButtonSMP
        _SelectButtonSMP = "/sp" + _SelectButtonSMP + iref;
        location.replace(_SelectButtonSMP);
    });
});

/*==================================================
　一記事のファーストビューに
　ユーティリティバーを重複表示させない。

--2015-03-26-17:30
==================================================*/
$(function() {
    var WindowHeightSize = $(window).height();
    var ArticleText = $("#Main .ArticleText");
    if ($(ArticleText).size() > 0) {
        var ArticleTop = $(ArticleText).position().top;
        var ArticleHeight = $(ArticleText).outerHeight();
        if ( ( WindowHeightSize - ArticleTop ) > ArticleHeight ) {
            //$(".SnsUtilityArea").hide();
        } else {
            $("#Main div.SnsUtilityArea").show();
        }
    }
});

/*==================================================
汎用モジュール　アコーディオン
--2015-07-30-14:30
==================================================*/
$(function(){
    $(".AccordionMod .AccordionAction").click(function() {
    $(this).prev().animate({height: 'toggle', opacity: 'toggle'},'slow');
    $(this).find('span').toggle();
});
    });
