"""
Identify xpaths which might contain ads using filter rules given a set of filter rules and a DOM

Example usage:

import codecs
import os
import pathlib
from ads_scraper.filter_rule_scraper import FilterRulesAdsScraper
from ads_scraper.constants import (
    FILTER_RULES_URLS_ALL,
)

TEST_FILE = "/".join(
    [
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

with codecs.open(TEST_FILE, "r") as f:
    html_code = f.read()

ads_parser = FilterRulesAdsScraper(
    html_source=html_code,
    filter_rules_url=FILTER_RULES_URLS_ALL,
    filter_rules_filename="filter_rules_all",
)

xpaths = ads_parser.find_ads_xpath()
print(xpaths)

"""

from io import StringIO
from cssselect import GenericTranslator
from lxml import etree
from .constants import FILTER_RULES_URLS_ALL
from .utils import get_filter_rules


class FilterRulesAdsScraper(object):
    """
    FilterRulesAdsScraper class identifies xpaths which might contain ads by converting Ad Blocker filter rules into xpaths
    
    args:
        html_source: target html source code
        filter_rules_urls: urls of filter rules docs to download or fetch, defaults to FILTER_RULES_URLS_ALL in ./constants.py
        other_relevant_xpaths: user defined xpaths
    """

    def __init__(
        self,
        html_source: str,
        filter_rules_url: str = FILTER_RULES_URLS_ALL,
        other_relevant_xpaths: list = [],
        **kwargs,
    ) -> None:
        self.filter_rules = get_filter_rules(
            filter_rules_url=filter_rules_url, **kwargs
        )
        self.filter_rules = self.css_to_xpath(self.filter_rules)
        # add other xpaths provided by the user
        if other_relevant_xpaths:
            self.filter_rules += other_relevant_xpaths
        self.rules_type = "xpath"
        parser = etree.HTMLParser()
        self.dom = etree.parse(StringIO(html_source), parser)

    def css_to_xpath(self, css_selectors:list) -> list:
        """ convert a css selector into xpath """
        out = []
        translator = GenericTranslator()
        for _ in css_selectors:
            try:
                out.append(translator.css_to_xpath(_))
            except:
                pass
        return out
        
    def get_element(self, xpath_query: str) -> etree._Element:
        """Fetch a web element using a xpath query"""
        return self.dom.xpath(xpath_query)

    def get_xpath(self, elem: etree._Element) -> str:
        """Get the xpath from an element"""
        return elem.getroottree().getpath(elem)

    def elem_is_descendant_of(
        self, target_elem: str, possible_parent_elem: str
    ) -> bool:
        """
        Given an element, transverse from it upwards until <body> or the possible parent element
        to identify whether the latter is an ascendent of the target node
        """
        current_elem = target_elem
        current_xpath = self.get_xpath(elem=current_elem)
        parent_xpath = self.get_xpath(elem=possible_parent_elem)
        matches = 0
        while current_elem.tag != "body" and matches == 0:
            current_elem = current_elem.getparent()
            if current_elem is not None:
                current_xpath = self.get_xpath(elem=current_elem)
                if current_xpath == parent_xpath:
                    matches += 1
        return matches > 0
    
    def find_ads_xpath(self) -> list:
        """
        Identify elements from the html which match xpaths from ad block filter rules.
        If a match occurs, grab also the parent node and the children.
        """
        ## create the xpath query by pipping the individual xpaths making it into one giant xpath (faster solution)
        rules_query = "|".join(self.filter_rules)
        matches = []
        ## look for web elements matching this query
        ads_elems = self.get_element(xpath_query=rules_query)
        if ads_elems:
            for elem in ads_elems:
                to_add = list()
                # store the maind elem
                to_add.append(self.get_xpath(elem))
                # add the parent node
                parent = elem.getparent()
                to_add.append(self.get_xpath(parent))
                # add the direct children
                children = elem.getchildren()
                to_add = to_add + [self.get_xpath(_) for _ in children]
                print(to_add)
                matches.append({str(i): xpath for i, xpath in enumerate(to_add)})
        return matches
