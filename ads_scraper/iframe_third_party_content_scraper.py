"""
Identify ads and third party content within iframes

Example usage:

import time
import os
import pathlib
from ads_scraper.utils import deploy_firefox
from ads_scraper.iframe_third_party_content_scraper import (
    IframeThirdPartyContentScraper,
)

TEST_FILE = "/".join(
    [
        "file://",
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

driver = deploy_firefox(headless=False)

driver.get(TEST_FILE)

time.sleep(10)
iframe_scraper = IframeThirdPartyContentScraper(
    driver=driver, url=TEST_FILE, repeat_n_times=3
)

ads_and_third_party_content = iframe_scraper.scrape_third_party_content()

print(ads_and_third_party_content)

driver.close()
driver.quit()

"""
from typing import Optional, Tuple
import requests
import time
import domain_utils as du
from adblockparser import AdblockRules
from bs4 import BeautifulSoup
from selenium.webdriver import Firefox
from .utils import get_filter_rules


class IframeThirdPartyContentScraper(object):
    """
    This class collects all the src and href attributes within elements inside of an <iframe>, checks whether an ad block would block them, and resolves them
    
    args:
        driver: selenium Firefox webdriver instance
        url: target url to use for third-party url identification
        repeat_n_times: number of times to scroll up and down the page during the iframe identification
        sleep_between_attempts: number of seconds to sleep in between repetitions
    """
    def __init__(
        self,
        driver: Firefox,
        url: str,
        repeat_n_times: int = 3,
        sleep_between_attempts: int = 10,
    ) -> None:
        self.driver = driver
        self.url = url
        raw_rules = get_filter_rules(raw=True)
        self.rules = AdblockRules(raw_rules)
        self.repeat_n_times = repeat_n_times
        self.sleep_between_attempts = sleep_between_attempts
        self.visited_domains = set()

    def is_ad_url(self, url: str) -> bool:
        """check if an adblocker would block a url"""
        return self.rules.should_block(url)

    def is_third_party_url(self, target_url: str) -> bool:
        """check if the eTLD of two urls is the same"""
        return du.get_etld1(target_url) != du.get_etld1(self.url)

    def fetch_all_urls(self, html_doc) -> list:
        """Fetch all the hred attributes from anchor tags"""
        soup = BeautifulSoup(html_doc, "html.parser")
        href_list = []
        for a in soup.find_all("a", href=True):
            if a["href"]:
                href_list.append(a["href"])
        return href_list

    def scrape_iframes(self) -> list:
        """scrape iframe web elements"""
        iframes_data = []
        for iframe in self.driver.find_elements_by_xpath("//iframe"):
            iframe_src = iframe.get_attribute("src")
            self.driver.switch_to.frame(iframe)
            if len(iframe_src) == 0:
                iframe_src = None
            urls_identified = self.fetch_all_urls(self.driver.page_source)
            iframes_data.append(
                {
                    "iframe_src": iframe_src,
                    "iframe_hrefs": urls_identified if urls_identified else None,
                }
            )
            self.driver.switch_to.default_content()
        return iframes_data

    def _resolve_url(self, url: str) -> Tuple[str, str]:
        """resolve a url"""
        try:
            resp = requests.get(url)
        except Exception as e:
            raise ValueError(f"HTTP error: {e}")
        return (resp.url, resp.text)

    def sanity_check(self, resp_content: str) -> bool:
        """run a sanity check by assessing if that url contains a body element"""
        # parse html
        soup = BeautifulSoup(resp_content, "html.parser")
        # first, must have a body
        if soup.find_all("body"):
            return True
        else:
            return False

    def resolve_url(self, url: str) -> Optional[str]:
        """resolve url and check if it has a body tag, if not return None"""
        resolved_url, resp_content = self._resolve_url(url=url)
        # sanity check
        if self.sanity_check(resp_content):
            return resolved_url

    def scroll_to_bottom(self) -> None:
        """ scroll to the bottom of the page"""
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")

    def slowly_scroll_down_to_bottom(self, speed: int = 8, sleep_time: int = 1):
        """ sequentially scroll down to the bottom of a page """
        current_scroll_position, new_height = 0, 1
        while current_scroll_position <= new_height:
            current_scroll_position += speed
            self.driver.execute_script(
                f"window.scrollTo(0, {current_scroll_position});"
            )
            new_height = self.driver.execute_script("return document.body.scrollHeight")
            time.sleep(sleep_time)

    def scroll_to_top(self) -> None:
        """ scroll to the top of a page """
        self.driver.execute_script("window.scrollTo(0,0)")
        
    def _scrape_third_party_content(self) -> list:
        """ 
        scrape all the href and src attributes from iframes.
        Resolve the resulting urls. 
        Check if they are from third-parties. If yes, append them them to a list and return. 
        """
        third_party_content = []
        # scrape iframes
        iframes_content = self.scrape_iframes()
        for iframes_dict in iframes_content:
            rel_urls = set()
            if not iframes_dict.get("iframe_hrefs"):
                if iframes_dict.get("iframes_src"):
                    rel_urls.update(iframes_dict.get("iframes_src"))
            else:
                if iframes_dict.get("iframe_hrefs"):
                    rel_urls.update(iframes_dict.get("iframe_hrefs"))
            if rel_urls:
                for _url in list(rel_urls):
                    if "http" in _url:
                        is_third_party = self.is_third_party_url(target_url=_url)
                        if is_third_party:
                            # check if it is an ad
                            is_ad = self.is_ad_url(url=_url)
                            try:
                                # resolve the url and check if the landing page has a body tag (none otherwise)
                                resolved = self.resolve_url(url = _url)
                            except:
                                resolved = None
                            if resolved:
                                domain = du.get_etld1(resolved)
                                # check if already visited
                                if domain not in list(self.visited_domains):
                                    self.visited_domains.add(domain)
                                    third_party_content.append(
                                        {
                                            "url": resolved,
                                            "is_ad": is_ad,
                                            "is_third_party": is_third_party,
                                        }
                                    )
        return third_party_content

    def scrape_third_party_content(self) -> list:
        """ main method """
        third_party_content = []
        for _ in range(1, self.repeat_n_times):
            self.slowly_scroll_down_to_bottom(speed=300)
            time.sleep(3)
            content_identified = self._scrape_third_party_content()
            third_party_content.extend(content_identified)
            self.scroll_to_top()
            time.sleep(self.sleep_between_attempts)
            if third_party_content:
                print(
                    f"[+] Iframe scraper found the following urls {','.join([_.get('url') for _ in third_party_content])}"
                )
        return third_party_content
