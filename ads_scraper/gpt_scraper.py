"""
Identify xpaths which might contain ads using google publisher tag api

Example usage:

import time
import os
import pathlib
from ads_scraper.utils import deploy_firefox
from ads_scraper.gpt_scraper import GooglePubTagScraper

TEST_FILE = "/".join(
    [
        "file://",
        os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "resources/samples/compressortech2/compressortech2.html",
        ),
    ]
)

driver = deploy_firefox(headless=False)

driver.get(TEST_FILE)

time.sleep(10)
gpt_scraper = GooglePubTagScraper(driver = driver)

xpaths = gpt_scraper.get_ad_container_xpaths()
print(xpaths)

driver.close()
driver.quit()
"""

from selenium.webdriver import Firefox
from typing import Optional


class GooglePubTagScraper(object):
    
    """
    This class leverages the Google Publisher Tag API (GPT) for identifing web-elements containing ad containers
    args:
        driver: selenium Firefox webdriver instance
    """
    def __init__(self, driver: Firefox) -> None:
        self.driver = driver

    def is_console_active(self) -> bool:
        return self.driver.execute_script(
            """
            if (window.googletag && googletag.apiReady) {
                return true
            } else {
                return false
            }
            """
        )

    def get_ad_container_ids(self) -> list:
        """Using the googletag manager js library fetch the id's of the elements containing the ads"""
        ad_container_list = []
        i = 0
        while True:
            try:
                elem_id = self.driver.execute_script(
                    f"return googletag.pubads().getSlots()[{i}].getSlotElementId()"
                )
            except:
                break
            ad_container_list.append(elem_id)
            i += 1
        return ad_container_list

    def get_ad_metadata(self) -> list:
        """For each ad slot, call the getResponseInformation() method so as to retrieve the campaign id, advertiser id and other data"""
        ad_metadata_list = []
        i = 0
        while True:
            try:
                elem_metadata = self.driver.execute_script(
                    f"return googletag.pubads().getSlots()[{i}].getResponseInformation()"
                )
            except:
                break
            
            ad_metadata_list.append(elem_metadata)
            i += 1
        return ad_metadata_list

    def make_ad_container_xpaths(self, ad_container_list: list) -> list:
        """given the ids of the ad containers, turn them into xpaths"""
        return [f"//*[@id='{ad_container}']" for ad_container in ad_container_list]

    def get_ad_container_xpaths(self) -> Optional[list]:
        """main method"""
        if self.is_console_active():
            ad_container_list = self.get_ad_container_ids()
            if ad_container_list:
                return self.make_ad_container_xpaths(
                    ad_container_list=ad_container_list
                )
