import os

# sleep times
SLEEP_SHORT = 1
SLEEP_MEDIUM = 3
SLEEP_LONG = 20

## Filter rules
EASYLIST_FILTER_RULES_URL_GEN = "https://easylist.to/easylist/easylist.txt"
EASYLIST_FILTER_RULES_URL_GEN_ELEM_HIDE = "https://raw.githubusercontent.com/easylist/easylist/master/easylist/easylist_general_hide.txt"
EASYLIST_FILTER_RULES_URL_DE = "https://easylist.to/easylistgermany/easylistgermany.txt"
EASYLIST_FILTER_RULES_URL_IT = (
    "https://easylist-downloads.adblockplus.org/easylistitaly.txt"
)
EASYLIST_FILTER_RULES_URL_NL = (
    "https://easylist-downloads.adblockplus.org/easylistdutch.txt"
)
EASYLIST_FILTER_RULES_URL_FR = "https://easylist-downloads.adblockplus.org/liste_fr.txt"
EASYLIST_FILTER_RULES_URL_BG = "https://stanev.org/abp/adblock_bg.txt"
EASYLIST_FILTER_RULES_URL_AR = "https://easylist-downloads.adblockplus.org/Liste_AR.txt"
EASYLIST_FILTER_RULES_URL_CS_SK = "https://raw.githubusercontent.com/tomasko126/easylistczechandslovak/master/filters.txt"
EASYLIST_FILTER_RULES_URL_LV = (
    "https://notabug.org/latvian-list/adblock-latvian/raw/master/lists/latvian-list.txt"
)
ADGUARD_FILTER_RULES_RU = "https://raw.githubusercontent.com/AdguardTeam/AdguardFilters/master/RussianFilter/sections/general_elemhide.txt"
ADGUARD_FILTER_RULES_EN = "https://github.com/AdguardTeam/AdguardFilters/blob/master/EnglishFilter/sections/general_elemhide.txt"
ADGUARD_FILTER_RULES_ES = "https://github.com/AdguardTeam/AdguardFilters/blob/master/SpanishFilter/sections/general_elemhide.txt"

FILTER_RULES_URLS_ALL = [
    EASYLIST_FILTER_RULES_URL_GEN,
    EASYLIST_FILTER_RULES_URL_DE,
    EASYLIST_FILTER_RULES_URL_IT,
    EASYLIST_FILTER_RULES_URL_NL,
    EASYLIST_FILTER_RULES_URL_FR,
    EASYLIST_FILTER_RULES_URL_BG,
    EASYLIST_FILTER_RULES_URL_AR,
    EASYLIST_FILTER_RULES_URL_CS_SK,
    EASYLIST_FILTER_RULES_URL_LV,
    ADGUARD_FILTER_RULES_RU,
    ADGUARD_FILTER_RULES_EN,
    ADGUARD_FILTER_RULES_ES,
]

# paths
PATH_TO_FILTER_RULES = "resources/filter_rules"
# path to geckodriver
PATH_TO_GECKODRIVER = "resources/geckodriver"
# path to geckodriver log 
GECKODRIVER_LOG = os.devnull

