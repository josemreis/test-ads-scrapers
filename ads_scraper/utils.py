"""
Utils used by ads_scraper.py and ads_parser.py
"""
import datetime
import json
import os
import random
import time
from typing import Callable, TypeVar
import requests
import re
from configparser import ConfigParser
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from .constants import (
    FILTER_RULES_URLS_ALL,
    GECKODRIVER_LOG,
    PATH_TO_FILTER_RULES,
    PATH_TO_GECKODRIVER,
)

T = TypeVar("T")


def get_paths(config_file: str = "config.ini") -> dict or None:
    """Get paths for binary and geckodriver"""
    cfg = ConfigParser()
    cfg.read(config_file)
    out = {}
    for section in cfg.sections():
        if section == "paths":
            out["executable"] = cfg.get(section, "GECKODRIVER")
            out["firefox_binary"] = cfg.get(section, "FIREFOX_BIN")
    return out


def deploy_firefox(
    path_to_geckodriver: str or None = None,
    headless: bool = False,
) -> webdriver.Firefox:
    """
    launches a firefox instance using the same browser version as in OpenWPM
    """
    path_to_geckodriver = path_to_geckodriver or PATH_TO_GECKODRIVER
    firefox_ops = Options()
    # run headlesss ?
    if headless:
        firefox_ops.add_argument("-headless")
    driver = webdriver.Firefox(
        executable_path=path_to_geckodriver,
        options=firefox_ops,
        service_log_path=GECKODRIVER_LOG,
    )
    return driver


def retry_with_backoff(
    function: Callable[[], T], retries: int = 3, backoff_base: int = 2
) -> T:
    """Retry a function with expontential backoff"""
    attempts = 0
    while True:
        try:
            return function()
        except Exception as e:
            if attempts == retries:
                raise e
            else:
                sleep = backoff_base**attempts + random.uniform(0, 1)
                print(f"error: '{e}', retrying in {round(sleep)} secs.")
                time.sleep(sleep)
                attempts += 1


def is_to_update(json_filename: str, update_every_n_days: int = 1) -> bool:
    """
    Check whether the filterules json files should be updated given its creation date and an update daily temporal window
    """
    ## default
    update = True
    if os.path.isfile(json_filename):
        # grab the date of file creation and transform it to datetime
        created_at = datetime.datetime.fromtimestamp(os.stat(json_filename).st_ctime)
        # date diff
        date_diff = datetime.datetime.today() - created_at
        if date_diff.days < update_every_n_days:
            update = False
        else:
            # remove older version
            os.remove(json_filename)
    return update


def parse_filter_rule(
    line: str,
    raw: bool = False,
    css_selectors_only: bool = False,
    domain_only: bool = False,
) -> str:
    if raw:
        return line
    elif css_selectors_only:
        if "##" in line:
            return line.split("##")[1]
    elif domain_only:
        if "href" in line:
            m = re.search('(?<=\=").*(?=")', line)
            if m:
                if m.group(0):
                    return m.group(0)
    else:
        return line


def _get_filter_rules(
    filter_rules_url: str or list = FILTER_RULES_URLS_ALL,
    css_selectors_only: bool = False,
    domain_only: bool = False,
    raw: bool = False,
) -> list:
    """
    Download and parse the filter rules.
    For more details on filter rules syntax see: https://kb.adguard.com/en/general/how-to-create-your-own-ad-filters#introduction
    """
    if not isinstance(filter_rules_url, list):
        filter_rules_url = [filter_rules_url]
    out = []
    for rules_url in filter_rules_url:
        # read by line and filter out comments and keep only css selectors
        out.extend(
            [
                parse_filter_rule(
                    line=line,
                    css_selectors_only=css_selectors_only,
                    domain_only=domain_only,
                    raw=raw,
                )
                for line in requests.get(rules_url).text.split("\n")
                if not line.startswith("!") and not line.startswith("[")
            ]
        )
    return out


def get_filter_rules(
    filter_rules_filename: str = "filter_rules_easylist_gen", **kwargs
) -> list:
    """
    Fetch filter rules stored in resources/filter-rules/ads-filter-rules or in a file online.
    For more details on filter rules syntax see: https://kb.adguard.com/en/general/how-to-create-your-own-ad-filters#introduction
    """
    if not os.path.isdir(PATH_TO_FILTER_RULES):
        os.mkdir(PATH_TO_FILTER_RULES)
    if "css_selectors_only" in kwargs:
        if kwargs.get("css_selectors_only"):
            filter_rules_filename = "_".join(
                [filter_rules_filename, "css_selectors_only"]
            )
    elif "domain_only" in kwargs:
        if kwargs.get("domain_only"):
            filter_rules_filename = "_".join([filter_rules_filename, "domain_only"])
    json_filename = os.path.join(PATH_TO_FILTER_RULES, filter_rules_filename + ".json")
    # if missing create the filter rules dir
    ## check if it exists need to update the rules
    to_update = is_to_update(json_filename=json_filename)
    if to_update:
        # download them and parse
        rules = _get_filter_rules(**kwargs)
        # turn to dict
        to_dict = {str(i): rules[i] for i in range(len(rules))}
        # write it as json
        with open(json_filename, "w", encoding="utf-8") as json_file:
            json.dump(to_dict, json_file, indent=4)
    # read it in, turn to list and retur
    with open(json_filename, "r", encoding="utf-8") as to_read:
        out = json.loads(to_read.read())
    out = list(out.values())
    return list(set(out))
