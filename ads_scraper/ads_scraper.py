import time
from typing import Optional
import domain_utils as du
import os
import re
import pandas as pd
import datetime
from selenium.webdriver import Firefox
from ads_scraper.iframe_third_party_content_scraper import (
    IframeThirdPartyContentScraper,
)
from .iframe_third_party_content_scraper import IframeThirdPartyContentScraper
from .filter_rule_scraper import FilterRulesAdsScraper
from .gpt_scraper import GooglePubTagScraper
from .constants import (
    SLEEP_LONG,
    SLEEP_MEDIUM,
    SLEEP_SHORT,
)  # faster: from constants import EASYLIST_FILTER_RULES_URL_GEN
from .utils import deploy_firefox

# basic sleep patterns to use between actions
SLEEP_SHORT = 1
SLEEP_MEDIUM = 3
SLEEP_LONG = 20

# number of times to scroll up and down the page and attempt to find ads
REPEAT_N_TIMES = 3
# number of seconds to sleep between ad identification attempts
SLEEP_BETWEEN_ATTEMPTS = 10


class AdsScraper(object):
    """
    AdsScraper class for scraping ads using a selenium webdriver and three methods:
        * Using the [Google Publisher Tag Manager API (GPT)](https://developers.google.com/publisher-tag) to identify the xpaths of ad containers
        * Matching web-element xpaths against xpaths from [ad blocker filter rules](ads_scraper/constants.py)
        * Using href and src attributes in elements within <iframes>
    args:
        url: target url for collecting ads data;
        crawler_id: user defined id for the crawler
        driver: selenium Firefox webdriver instance
        sleep_at_arrival: number of seconds to sleep as arriving on the page
        use_gpt: use GooglePubTagScraper for identifying ads xpaths?
        use_filter_rule_scraper: use FilterRulesAdsScraper for identifying ads xpaths?
        use_iframe_third_party_content_scraper: use IframeThirdPartyContentScraper for identifying third-party content and ads?
        **kwargs -> additional args to add to the FilterRulesAdsScraper
    """

    def __init__(
        self,
        url: str,
        crawler_id: str = f"crawler_{datetime.datetime.now().strftime('%d_%M_%Y_%H_%M_%S')}",
        driver: Firefox or None = None,
        sleep_at_arrival: int = SLEEP_LONG,
        use_gpt: bool = True,
        use_filter_rule_scraper: bool = True,
        use_iframe_third_party_content_scraper: bool = True,
        debug: bool = True,
        **kwargs,
    ) -> None:
        self.debug = debug
        self.driver = driver or deploy_firefox()
        self.crawler_id = crawler_id
        # go to the page if not there already
        if self.driver.current_url != url:
            self.driver.get(url)
            time.sleep(sleep_at_arrival)
        # maximize window size of browser client
        self.driver.maximize_window()
        time.sleep(SLEEP_SHORT)
        # original window handle
        self.original_window_handle = self.driver.current_window_handle
        # landing page of user provided url url
        self.url = self.driver.current_url
        # successful xpaths list container
        self.successful_xpaths = []
        # visited url domain names list container
        self.visited_domains = []
        # use Google Publisher Tag Manager API scraper
        self.use_gpt = use_gpt
        # use filter rule based scraper
        self.use_filter_rules_scraper = use_filter_rule_scraper
        # use the iframe third-party content scraper
        self.use_iframe_scraper = use_iframe_third_party_content_scraper
        ## add the remaining arguments for use filter rule scraper
        self.filter_rules_scraper_kwargs = kwargs

    def _get_ad_xpaths_using_gpt(self) -> list:
        """Fetch the xpaths to the elements containing the ads using the google publisher tag API"""
        ## create an instance of the google publisher tag scraper
        if self.debug:
            print(f"[+] Looking for gpt ad containers")
        return GooglePubTagScraper(driver=self.driver).get_ad_container_xpaths()

    def _get_ad_xpaths_using_filter_rule_scraper(self) -> list:
        """Fetch xpaths of elements matching ad blocker filter rules, as well as the paths to its ascendant and descendant elements"""
        if self.debug:
            print(f"[+] Looking for elements matching Ad Blockers' filter rules")
        filter_rules_scraper = FilterRulesAdsScraper(
            html_source=self.driver.page_source, **self.filter_rules_scraper_kwargs
        )
        return filter_rules_scraper.find_ads_xpath()

    def get_ad_xpaths(self) -> list:
        """wraper around _get_ad_xpaths_using_gpt and _get_ad_xpaths_using_filter_rule_scraper"""
        out = []
        if self.use_gpt:
            out.extend(self._get_ad_xpaths_using_gpt())
        if self.use_filter_rules_scraper:
            out.extend(self._get_ad_xpaths_using_filter_rule_scraper())
        return out

    def xpath_is_descendant_of(
        self, target_xpath: str, possible_parent_xpath: str
    ) -> bool:
        """
        Given an xpath, transverse it upwards until <body> or the possible parent element's xpath
        to identify whether the latter is an ascendent of the target xpath
        """
        try:
            current_elem = self.get_element(target_xpath)[0]
        except:
            return False
        current_xpath = target_xpath
        parent_xpath = possible_parent_xpath
        matches = 0
        while current_elem.tag != "body" and matches == 0:
            current_elem = current_elem.getparent()
            if current_elem is not None:
                current_xpath = self.get_xpath(elem=current_elem)
                if current_xpath == parent_xpath:
                    matches += 1
        return matches > 0

    def is_related_or_self_of_successful_xpath(self, target_xpath: str) -> bool:
        """
        A successful xpath is one with which we managed to open an ad (they are stored as the class attributes self.successful_xpaths.
        To avoid "double clicking" on the same ad, we check whether the target_xpath is related or equal to a successful xpath.
        """
        out = False
        if self.successful_xpaths:
            is_self = target_xpath in self.successful_xpaths
            is_descendant = any(
                [
                    self.xpath_is_descendant_of(
                        target_xpath=target_xpath, possible_parent_xpath=_
                    )
                    for _ in self.successful_xpaths
                ]
            )
            is_ascendant = any(
                [
                    self.xpath_is_descendant_of(
                        target_xpath=_, possible_parent_xpath=target_xpath
                    )
                    for _ in self.successful_xpaths
                ]
            )
            out = any([is_self, is_descendant, is_ascendant])
        return out

    def fetch_href(self, elem) -> str or None:
        """Fetch a href attribute from a webelement"""
        return elem.get_attribute("href")

    def open_ad(self, ads_xpaths: list) -> str:
        """
        given a dictionary with tentative ad elements, try to open it.
        Return the new window handle and xpaths (self and parent)
        """
        if isinstance(ads_xpaths, dict):
            ads_xpaths = list(ads_xpaths.values())
        elif isinstance(ads_xpaths, str):
            ads_xpaths = [ads_xpaths]
        # grab the initial window count
        initial_window_count = len(self.driver.window_handles)
        window = None
        # if not self or descendant of an xpath which opened an ad, go on; else, skip it to avoid double clicking
        ads_elem = None
        for ads_xpath in ads_xpaths:
            if not self.is_related_or_self_of_successful_xpath(target_xpath=ads_xpath):
                try:
                    # find the element
                    ads_elem = self.driver.find_element_by_xpath(ads_xpath)
                    if self.debug:
                        print(f"\t[+] Found a possible ad at '{ads_xpath}'")
                    # check if it has a link in a href attribute
                    link = self.fetch_href(elem=ads_elem)
                    if link:
                        # already visited or internal? if yes, skip
                        if self.is_already_visited(url=link):
                            if self.debug:
                                print(f"\t\t[!] Already visited, skipping it.")
                            continue
                    self.driver.execute_script(
                        "arguments[0].scrollIntoView();", ads_elem
                    )
                    time.sleep(SLEEP_MEDIUM)
                    # try to move to the element and click on it
                    if self.debug:
                        print(f"\t\t[+] Attempting to click on the element")
                    ads_elem.click()
                except:
                    pass
                if ads_elem is not None:
                    if len(self.driver.window_handles) > initial_window_count:
                        # click worked. Return the new window handle
                        if self.debug:
                            print(f"\t\t[+] Click worked, opened a new window")
                        window = [
                            window
                            for window in self.driver.window_handles
                            if window != self.original_window_handle
                        ][0]
                        # ad xpaths (self and parent) to the successful xpaths
                        self.successful_xpaths.extend(ads_xpaths)
                        break
            else:
                if self.debug:
                    print(
                        f"\t\t[!] '{ads_xpath}' is related or self of a previously used xpath, skipping it"
                    )
        return window

    def wait_for_new_page_load(self, timeout: int = SLEEP_LONG) -> None:
        """Hacky way of waiting for the new page to load"""
        secs_passed = 0
        while self.driver.current_url == self.url:
            if secs_passed < timeout:
                time.sleep(1)
                secs_passed += 1
            else:
                raise ValueError("Timeout while loading the page.")

    def collect_ad_data(self, new_window_handle: str) -> dict:
        """Switch to the ad window and collect url and page source"""
        # go to the page
        time.sleep(SLEEP_SHORT)
        self.driver.switch_to.window(new_window_handle)
        self.wait_for_new_page_load(timeout=SLEEP_LONG)
        if self.debug:
            print(f"\t\t[+] Switched to a new window")
        time.sleep(SLEEP_MEDIUM)
        # fetch the data
        if self.debug:
            print(f"\t\t[+] Collecting the url")
        ad_data = {
            "url": self.driver.current_url,
            "is_ad": True,
            "is_third_party": du.get_etld1(self.driver.current_url)
            != du.get_etld1(self.url),
        }
        # close it
        self.driver.close()
        time.sleep(SLEEP_SHORT)
        # return to the main window handle
        self.driver.switch_to.window(self.original_window_handle)
        return ad_data

    def is_already_visited(self, url: str) -> bool:
        """fetch the domain name of the url and try to find it in the list of domain names already visited and check that it is not an internal link"""
        target_domain = du.get_etld1(url)
        return target_domain in self.visited_domains and target_domain != du.get_etld1(
            self.url
        )

    def _get_ads_using_xpath_based_scrapers(self, xpaths: list) -> list:
        """collect"""
        ads_data = []
        for ads_xpaths in xpaths:
            # try to open an add
            new_window = self.open_ad(ads_xpaths=ads_xpaths)
            if new_window:
                # opened it, collect the data
                ad_data = self.collect_ad_data(new_window_handle=new_window)
                # double check that we did not add this one already
                if ad_data["url"] != "about:blank":
                    if not self.is_already_visited(url=ad_data["url"]):
                        if self.debug:
                            print(
                                f'\t[+] Checking if "{ad_data["url"]}" was already visited'
                            )
                        self.visited_domains.append(du.get_etld1(ad_data["url"]))
                        ads_data.append(ad_data)
        return ads_data

    def get_ads_using_xpath_based_scrapers(
        self,
        xpaths: list,
        repeat_n_times: int = REPEAT_N_TIMES,
        sleep_between_attempts: int = SLEEP_BETWEEN_ATTEMPTS,
    ) -> list:
        """collect"""
        ads_data = []
        for _ in range(repeat_n_times):
            ads_data.extend(self._get_ads_using_xpath_based_scrapers(xpaths=xpaths))
            time.sleep(sleep_between_attempts)
        if self.debug:
            msg = f"[+] In {self.url} found"
            if ads_data:
                msg += f" the following ads:{os.linesep}{os.linesep.join(map(str, ['    [+] ' + ad['url'] for ad in ads_data]))}"
            else:
                msg += " no ads"
            print(msg)
        return ads_data

    def _get_ads_using_iframe_third_party_content_scraper(self) -> list:
        """Fetch ads using the iframe third party content scraper"""
        if self.debug:
            print(
                f"[+] Looking for third-party and ad urls within href and src attributes within iframes"
            )
        iframe_scraper = IframeThirdPartyContentScraper(
            driver=self.driver,
            url=self.url,
            repeat_n_times=REPEAT_N_TIMES,
            sleep_between_attempts=SLEEP_BETWEEN_ATTEMPTS,
        )
        ad_data = iframe_scraper.scrape_third_party_content()
        if ad_data:
            self.visited_domains.extend([du.get_etld1(d["url"]) for d in ad_data])
        return ad_data

    def sort_and_keep_unique_ads(self, ads_list: list) -> list:
        """
        Takes a list of dictionaries with the following format:
        [
            {
            "url": "https://www.gastechevent.com/conferences/strategic-conference/?utm_source=COMPRESSORtech2&utm_medium=web+banner&utm_campaign=Gastech+2022&utm_id=delprom&utm_term=May&utm_content=Early+bird",
            "is_ad": false,
            "is_third_party": true
            }
        ]
        Converts it to a dataframe, arranges by the column "is_ad" so that priority is given to urls identified as ads, and then keeps unique ad urls

        see also: https://docs.python.org/3/reference/datamodel.html#the-standard-type-hierarchy
        """
        return (
            pd.DataFrame(ads_list)
            .sort_values(by="is_ad", axis=0, ascending=False)
            .drop_duplicates("url")
            .to_dict("records")
        )

    def methods_used(self) -> dict:
        """fetch the user selected ads scraper methods related attributes and return as dict"""
        methods_used = {}
        for attribute, value in self.__dict__.items():
            if re.match("^use\_", attribute):
                methods_used[attribute.split("use_")[1]] = value
        return methods_used

    def scrape_ads(self) -> Optional[list]:
        """
        scrape ads using the iframe ads scraper or the xpath based scrapers
        """
        if self.debug:
            print(f"[+] Start scraping: '{self.url}'")
        # container lists
        ads_data = []
        # get ads using the gpt
        if self.use_gpt:
            gpt_xpaths = self._get_ad_xpaths_using_gpt()
            if gpt_xpaths:
                ads_data.extend(
                    self.get_ads_using_xpath_based_scrapers(xpaths=gpt_xpaths)
                )
        # get ads using the filter rule scraper
        if self.use_filter_rules_scraper:
            fr_xpaths = self._get_ad_xpaths_using_filter_rule_scraper()
            if fr_xpaths:
                ads_data.extend(
                    self.get_ads_using_xpath_based_scrapers(xpaths=fr_xpaths)
                )
        # add the ads identified by the iframe scraper
        if self.use_iframe_scraper:
            ads_data.extend(self._get_ads_using_iframe_third_party_content_scraper())
        if ads_data:
            # keep unique while giving priority to entries where is_ad == True
            ads = self.sort_and_keep_unique_ads(ads_data)
            # add the visited website
            out = []
            for ads_dict in ads:
                ads_dict["website_visited"] = self.url
                ads_dict["visited_at"] = datetime.datetime.now().isoformat()
                ads_dict["crawler_id"] = self.crawler_id
                ads_dict["methods_used_for_ad_identification"] = self.methods_used()
                out.append(ads_dict)
            return out
